package Learning2;

public class binarySearch {
    public static void main(String[] args) {
        int[] list = {4, 5, 1, 44, 2, -3, -7, 10, 99};
        int temp = 0;

        //Сортировка от меньшего к большему (мой вариант)
       for (int i = 0; i < list.length - 1; i++) {
            for (int j = i + 1; j < list.length - 1; j++) {
                if (list[i] > list[j]) {
                    temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                }
            }
        }

        System.out.println("Индекс значения 2 в массиве является равным: " + binarySearch(list, 10) +
                " из " + (list.length - 1) + " индексов массива.");
    }

    //метод бинарного поиска ключа в массиве
    public static int binarySearch(int[] list, int key) {
        int low = 0;
        int high = list.length - 1;

        while (high >= low) {

            //Если high является очень большим целым числом, например,
            // таким как максимальное значение 2147483647 в диапазоне типа int,
            // то (low + high) / 2 может привести к переполнению.
            // Как избежать переполнения?
            // Заменить (low + high) / 2 на (-low + high) / 2 + low.

            //int mid = (low + high) / 2;

            int mid = (-low + high) / 2 + low;
            if (key < list[mid])
                high = mid - 1;
            else if (key == list[mid])
                return mid;
            else
                low = mid + 1;
        }
        return -low - 1; // Теперь high < low
    }

}
