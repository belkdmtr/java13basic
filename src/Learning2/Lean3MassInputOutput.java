package Learning2;

public class Lean3MassInputOutput {
    public static void main(String[] args) {

        //Определение массивов и очень интересное получение из него значений!!!!!!

        double[] myList = new double[3]; //массив создан

        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.print("Введите " + myList.length + " значений массива! \n");
        for (int i = 0; i < myList.length; i++) {
            System.out.print("Элемент массива № " + i + ": ");
            myList[i] = input.nextDouble();
        }
        for (double v : myList) { // аналог for (int i = 0; i < myList.length; i++)
            System.out.print(v + " \t");
        }
    }
}
