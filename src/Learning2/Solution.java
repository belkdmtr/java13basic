package Learning2;

//Напишите программу, которая вычисляет полную стоимость товара.
// Данные, используемые для расчета: стоимость единицы товара в рублях,
// количество единиц товара, включение/не включение НДС (20%) в стоимость товара.
// Если количество единиц товара больше или равно 10, то действует скидка 5%.

import java.util.Scanner;

public class Solution {
    public static final double NDS = 20;
    public static final int SALE = 5; //Если количество единиц товара больше или равно 10, то действует скидка 5%
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String includeVatResponse; // включение НДС в стоимость товара

        System.out.print("Введите стоимость товара в рублях: ");
        double costProd = scanner.nextDouble(); //Cтоимость единицы товара в рублях.

        System.out.print("Введите количество товара ");
        int countProd = scanner.nextInt(); //Количество единиц товара.

        //Цикл пока не введем Y или N будет выполняться
        do {
            System.out.println("Включать НДС в стоиомть товара (y/n): ");
            includeVatResponse = scanner.next();
        } while (!"y".equalsIgnoreCase(includeVatResponse) && !"n".equalsIgnoreCase(includeVatResponse));

        if (includeVatResponse.equals("y")) {
            if (countProd >= 10) {
                System.out.println("Стоимость товара с НДС " +
                        "и со скидкой 5%: " + getFullCost(costProd, countProd, SALE, NDS));
            } else {
                System.out.println("Стоимость товара " +
                        "с НДС без скидки: " + getFullCost(costProd, countProd, NDS));
            }
        } else {
            if (countProd >= 10) {
                System.out.println("Стоимость товара без " +
                        "НДС и со скидкой 5%: " + getFullCost(costProd, countProd, SALE));
            } else {
                System.out.println("Стоимость товара без " +
                        "НДС и без скидки: " + getFullCost(costProd, countProd));
            }
        }
    }

    //Метод получения полной стоимости товара
    public static double getFullCost(double cost, int count) {
        return cost * count;
    }

    //Метод вычисления полной стоимости товара c учетом скидки 5%
    public static double getFullCost(double cost, int count, int sale) {
        double costWithSale = cost * (1 - sale / 100.0) * count;
        return Math.round(costWithSale * 100) / 100.0; // Окургление до копеек.
    }

    //Метод вычисления полной стоимости товара включая НДС но без скидки;
    public static double getFullCost(double cost, int count, double nds) {
        double costWithNds = (cost * count) * (1 + nds / 100);
        return Math.round(costWithNds * 100) / 100.0; // Окургление до копеек.
    }

    //Метод вычисления полной стоимости товара включая НДС и со скидкой;
    public static double getFullCost(double cost, int count, int sale, double nds) {
        double costWithSale = cost * (1 - sale / 100.0) * count;
        double costWithNdsSale = costWithSale * (1 + nds / 100);
        return Math.round(costWithNdsSale * 100) / 100.0; // Окургление до копеек.
    }
}
