package Learning2;

public class Learn4MassIndex {
    public static void main(String[] args) {

        int[] myList = {1, 5, 3, 4, 7, 5};

        int max = myList[0];

        int indexOfMax = 0;
        for (int i = 0; i < myList.length; i++) {
            if (myList[i] > max) {
                max = myList[i];
                indexOfMax = i;
            }
        }
        System.out.println(indexOfMax); //Выводит индекс массива содержащий максимальное значение;
    }
}
