package Learning2;

public class Java5MassRandom {
    public static void main(String[] args) {
        int[] myList = {1, 5, 3, 4, 7, 5};

        for (int i = 0; i < myList.length; i++) {
            //Сгенерировать случайный индекс
            int j = (int)(Math.random() * myList.length);

            //Переставить myList[i] и myList[j]
            int temp = myList[i];
            myList[i] = myList[j];
            myList[j] = temp;
        }

        //выводим значения массива
        for (int v : myList) { // аналог for (int i = 0; i < myList.length; i++)
            System.out.print(v + " \t");
        }
    }
}
