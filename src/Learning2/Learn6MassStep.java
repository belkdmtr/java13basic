package Learning2;

public class Learn6MassStep {
    public static void main(String[] args) {

        double[] myList = {1, 5, 3, 4, 7, 5};

        double temp = myList[0]; // Сохраняет первый элемент

// Сдвинуть элементы влево
        for (int i = 1; i < myList.length; i++) {
            myList[i - 1] = myList[i];
        }
// Переместить значение первого элемента, чтобы заполнить последнюю позицию
        myList[myList.length - 1] = temp;

    }
}
