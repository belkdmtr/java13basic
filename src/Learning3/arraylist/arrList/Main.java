package Learning3.arraylist.arrList;
import java.util.ArrayList;

/**
 * Задание
 * Напишите фрагменты кода для выполнения следующих операций:
 * 1. Создайте ArrayList для хранения значений типа double.
 * 2. Добавьте в ArrayList новый объект.
 * 3. Вставьте новый объект в начало ArrayList.
 * 4. Найдите количество объектов в ArrayList.
 * 5. Удалите указанный объект из ArrayList.
 * 6. Удалите последний объект из ArrayList.
 * 7. Проверьте, находится ли указанный объект в ArrayList.
 * 8. Извлеките объект с указанным индексом из ArrayList.
 */

public class Main {
    public static void main(String[] args) {

        //Создайте ArrayList для хранения значений типа double.
        ArrayList<Double> list = new ArrayList<>();
        //Добавьте в ArrayList новый объект.
        list.add(5.0);
        list.add(3.0);
        System.out.println("Array: " + list.toString());
        //Вставьте новый объект в начало ArrayList.
        list.add(0, 1.0);
        //Найдите количество объектов в ArrayList.
        System.out.println("Array after add 1.0: " + list.toString());
        list.set(0, 9.0);
        System.out.println("Array after set 9.0: " + list.toString());
        System.out.println(list.size());
        //Удалите указанный объект из ArrayList.
        list.remove(0);
        //Удалите последний объект из ArrayList.
        list.remove(list.size() - 1);
        //Проверьте, находится ли указанный объект в ArrayList.
        System.out.println(list.contains(5.0));
        //Извлеките объект с указанным индексом из ArrayList.
        System.out.println(list.get(0));

        //Удалить число 5.0 из списка?
        list.remove(new Double(5.0)); // если был ло бы целое число Integer
        System.out.println("Array after delet 5.0: " + list.toString()); //Масств оказывается пустым

        //ArrayList<String> list = new ArrayList<>(Arrays.asList(array));
        //ArrayList<Integer> list = new ArrayList<>(Arrays.asList(array));













    }
}
