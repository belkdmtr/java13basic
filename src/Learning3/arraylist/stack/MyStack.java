package Learning3.arraylist.stack;

import java.util.ArrayList;

public class MyStack {

    //Здесь для хранения элементов в стеке создается список
    private ArrayList<Object> list = new ArrayList<>();

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int getSize() {
        return list.size();
    }

    //Метод peek() (строчки №14-16) возвращает элемент на вершине стека без удаления его.
    //Вершина стека находится в конце списка.

    public Object peek() {
        return list.get(getSize() - 1);
    }

    //Метод pop() «выталкивает» (удаляет) верхний элемент из стека и возвращает его.
    public Object pop() {
        Object o = list.get(getSize() - 1);
        list.remove(getSize() - 1);
        return o;
    }

    //Метод push(Object element) «заталкивает» (добавляет) указанный элемент на вершину стека.
    public void push(Object o) {
        list.add(o);
    }

    //Метод toString() класса Object переопределяется для отображения содержимого стека
    //с помощью вызова метода list.toString(). Метод toString(),
    //реализованный в ArrayList, возвращает строковое представление всех элементов списка.
    @Override /** Переопределяет метод toString класса Object */
    public String toString() {
        return "стек: " + list.toString();
    }

    /*

    // MyStack.java: Реализация стека с помощью наследования
class MyStack extends java.util.ArrayList<Object> {
  public boolean isEmpty() {
    return super.isEmpty();
  }

  public int getSize() {
    return size();
  }

  public Object peek() {
    return get(getSize() - 1);
  }

  public Object pop() {
    return remove(getSize() - 1);
  }

  public Object push(Object o) {
    add(o);
    return o;
  }

//  public int search(Object o) {
//    return indexOf(o);
//  }

  public String toString() {
    return "стек: " + toString();
  }
}


    */

}


