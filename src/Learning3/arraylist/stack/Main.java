package Learning3.arraylist.stack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        MyStack myStack = new MyStack();
        System.out.println(myStack.isEmpty());

        //Запрашивает у пользователя пять строк и отображает их в обратном порядке.
        for (int i = 0; i < 5; i ++) {
            myStack.push(input.nextLine());
        }

        System.out.println(myStack.getSize() + " строки: ");
        while (!myStack.isEmpty()){
            System.out.println(myStack.pop());
        }





        System.out.println(myStack.toString());






    }
}
