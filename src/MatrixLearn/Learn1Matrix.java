package MatrixLearn;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Learn1Matrix {
    public static void main(String[] args) {

        java.util.Scanner input = new java.util.Scanner(System.in);
        int[][] matrix = new int[3][2];

        /*double[][] distances = {
                {1, 2, 3, 4, 5},
                {2, 3, 4, 5},
                {3, 4, 5},
                {4, 5},
                {5},
        };*/

        System.out.println("Введите " + matrix.length + " строчек и " +
                matrix[0].length + " столбцов: ");


        //заполнить массив
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                System.out.print("Строка: " + row + " столбец " + column + " ");
                matrix[row][column] = input.nextInt();
            }
        }

        //отобразить массив
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                System.out.print(matrix[row][column] + " ");
            }
            System.out.println();
        }


    }
}
