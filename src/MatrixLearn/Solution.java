package MatrixLearn;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfClients; //Количество клиентов;
        Object[][] clients; //Массив!!!!! Охуенно объяснили, откуда я должен это знать?

        //1. Вводим количество клиентов.
        System.out.print("Введите количество клиентов: ");
        countOfClients = scanner.nextInt();

        //2. Инициализация массива Object;
        clients = new Object[countOfClients][4]; //4 потому что - Фамилия / Имя / Отчество / Баланс счета

        //3. Заполним данные о клиенте
        //Иванов Иван Иванович 3991
        //Сидоров Сидр Сидорович 9300

        for (int i = 0; i < countOfClients; i++) {
            // Записываем фамилию в колонку i с индексом 0
            System.out.print("Введите Фамилию: ");
            clients[i][0] = scanner.next();

            // Записываем имя в колонку i с индексом 1
            System.out.print("Введите Имя: ");
            clients[i][1] = scanner.next();

            // Записываем отчество в колонку i с индексом 2
            System.out.print("Введите Отчество: ");
            clients[i][2] = scanner.next();

            // Записываем юаданс в колонку i с индексом 3
            System.out.print("Введите бюаланс на счете: ");
            clients[i][3] = scanner.next();
        }

        // АЛГОРИТМ СОРТИРОВКИ ПУЗЫРЬКОМ!!!!!!!!!!!!!!
        //С помощью двух циклов for выполним проходы по внутреннему и внешнему циклам.
        for (int i = 0; i < clients.length; i++) { // Внешний цикл
            for (int j = 0; j < clients.length - 1; j++) { // Внутренний цикл
                //Для получения значения  баланса счета воспользуемся приведением объекта
                // типа Object к типу Double.
                // Далее приведем к соответствующему примитиву double
                // — это называется распаковкой или unboxing.
                //
                // первый элемент в паре
                // преобразуем объект типа Object к типу Double
                // и преобразуем к соответствующему примитиву double
                double firstInPair = Double.parseDouble((String) clients[j][3]);
                // второй элемент в паре
                double secondInPair = Double.parseDouble((String) clients[j + 1][3]);
                //double secondInPair = (Double) clients[j + 1][3];

                // Сравниваем элементы в паре, второй элемент должен быть больше
                if(firstInPair > secondInPair) {
                    // Если порядок элементов нарушен
                    // меняем строки местами
                    swap(clients, j, j + 1);
                }
            }
        }

        // Вывести отсортированные данные пользователей
        System.out.println("Данные клиентов в порядке увеличения баланса счета:");
        for (int i = 0; i < clients.length; i++) {

            // Формируем строку по каждому клиенту
            // Фамилия
            String surnameWithInitials = clients[i][0] + " ";
            // Инициал имени
            surnameWithInitials += clients[i][1].toString().substring(0, 1) + ".";
            // инициал отчества
            surnameWithInitials += clients[i][2].toString().substring(0, 1) + ".";
            // Баланс счета
            surnameWithInitials += " " + clients[i][3];
            System.out.println(surnameWithInitials);
        }


    } //ТУТ ЗАКАНЧИВАЕТСЯ MAIN

    /**
     * Обмен строк с индексами i и j местами в двумерном массиве array
     */
    static void swap(Object[][] array, int i, int j) {
        // Обмениваем строки местами
        for (int k = 0; k < array[i].length; k++) { // Итерируемся по элементам двух строк
            // буферный элемент в котором хранится значение элемента массива строки с индексом i
            Object buff = array[i][k];
            // в ячейку строки с индексом i записываем значение из ячейки строки с индексом j той же колонки
            array[i][k] = array[j][k];
            // в ячейку строки с индексом j записываем значение из буфера
            array[j][k] = buff;
        }
    }


}
