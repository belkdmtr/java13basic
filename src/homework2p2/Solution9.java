package homework2p2;

import java.util.Scanner;

//На вход подается число N. Необходимо вывести цифры числа слева направо.
//Решить задачу нужно через рекурсию.

public class Solution9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Получить сумму цифр числа через рекурсию

        int n = input.nextInt();
        String str = "";

        str = summNum(n, str);
        System.out.print(str.substring(0, str.length() - 1));
    }


    //Метод для реализации поиска суммы числа чероез рекурсию
    public static String summNum (int n, String str) {
        if (n <= 0) {
            return str;
        }
        str = n % 10 + " " + str;
        return summNum( n  / 10, str);
    }
}
