package homework2p2;

/**
 * На вход подается число N — количество строк и столбцов матрицы. Затем в
 * последующих двух строках подаются координаты X (номер столбца) и Y (номер
 * строки) точек, которые задают прямоугольник.
 * Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
 * заполненной нулями (см. пример) и вывести всю матрицу на экран.
 */

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] mass = new int[n][n];

        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int xmax = scanner.nextInt();;
        int ymax = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == xmax && j ==y) {
                    mass[i][j] = 0;
                } else {
                    if (j >= x && j <= xmax && i >= y && i <= ymax) {
                        mass[i][j] = 1;
                    } else {
                        mass[i][j] = 0;
                    }
                }

            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j != n - 1) {
                    System.out.print(mass[i][j] + " ");
                } else {
                    System.out.print(mass[i][j]);
                }
            }
            System.out.println();
        }
    }

    /*public static void mass (int inI, int inJ) {
        for ()
    }*/



}

