//package homework2p2;

import java.util.Scanner;

/**
 * На вход подается число N — количество строк и столбцов матрицы.
 * Затем передается сама матрица, состоящая из натуральных чисел.
 * Необходимо вывести true, если она является симметричной относительно
 * побочной диагонали, false иначе.
 * Побочной диагональю называется диагональ, проходящая из верхнего правого
 * угла в левый нижний.
 */

public class Solution5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[][] mass = new int[n][n]; //[кол-во_строк][кол-во_столбцов]
        boolean flag = false;

        //Заплоним массив
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                mass[i][j] = scanner.nextInt();
            }
        }


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j && mass[i][j] == mass[(n - 1) - i][(n - 1) - j]) {
                    flag = true;
                } else {
                    flag = false;
                }
            }
        }
        System.out.println(flag);
    }
}

/**

 false
 3
 1 2 3
 4 5 6
 7 8 9

 true
 5
 57 190 160 71 42
 141 79 187 19 71
 141 16 7 187 160
 100 42 16 79 190
 15 100 141 141 57

 */
