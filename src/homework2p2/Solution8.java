package homework2p2;

//На вход подается число N. Необходимо посчитать и вывести на экран сумму его
//цифр. Решить задачу нужно через рекурсию.

import java.util.Scanner;

public class Solution8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Получить сумму цифр числа через рекурсию

        int n = input.nextInt();
        int sum = 0;

        System.out.print(summNum(n, sum));
    }


    //Метод для реализации поиска суммы числа чероез рекурсию
     public static int summNum (int n, int sum) {
        if (n <= 0) {
            return sum;
        }
        sum += n % 10;
        return summNum( n  / 10, sum);

    }


    /**
     * Метод для реализации поиска суммы числа
     * public static int summNum (int n) {
     *         int sum = 0;
     *         while (n > 0) {
     *             sum += n % 10;
     *             n = n / 10;
     *         }
     *         return sum;
     *     }
     */


}
