//package homework2p2;

import java.util.Scanner;

public class Solution1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt(); //строка
        int n = scanner.nextInt(); //колонка
        int[][] mass = new int[n][m];
        //System.out.println(mass.length);

        //Заполним массив
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mass[i][j] = scanner.nextInt();
            }
        }


        //вывод массива

        int min;
        int count = 0;
        String str = "";

        for (int i = 0; i < n; i++) {
            min = mass[i][count];
            for (int j = 0; j < m; j++) {
                if (mass[i][j] < mass[i][count]) {
                    min += mass[i][j];
                }
            }
            if (m != 1) { //что бы за пределыв не выходило при n = 1;
                count++;
            }
            //System.out.print(min + " ");
            str += min + " ";
        }

        System.out.print(str.substring(0, str.length() - 1));

    }
}
