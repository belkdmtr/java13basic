package homework2p2;

/**
 * Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
 * хранения участников и оценок неудобная, а победителя определить надо. В
 * первой таблице в системе хранятся имена хозяев, во второй - клички животных,
 * в третьей — оценки трех судей за выступление каждой собаки. Таблицы
 * связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
 * строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
 * помочь Пете определить топ 3 победителей конкурса.
 * На вход подается число N — количество участников конкурса. Затем в N
 * строках переданы имена хозяев. После этого в N строках переданы клички
 * собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
 * оценки судей. Победителями являются три участника, набравшие
 * максимальное среднее арифметическое по оценкам 3 судей. Необходимо
 * вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
 * Гарантируется, что среднее арифметическое для всех участников будет
 * различным.
 */

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Solution7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(); //количество участников = 4
        String[] name = new String[n]; //имена хозяев = 4
        String[] dogs = new String[n]; //клички собак = 4
        int[][]  scores = new int[n][3]; //[кол-во_строк][кол-во_столбцов]
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        decimalFormat.setRoundingMode(RoundingMode.DOWN);

        //Заплоним массив 1
        for (int i = 0; i < n; i++) {
           name[i] = scanner.next();
        }

        //Заплоним массив 2
        for (int i = 0; i < n; i++) {
            dogs[i] = scanner.next();
        }

        //Заплоним массив 3
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                scores[i][j] = scanner.nextInt();
            }
        }

        int temp = 0;
        int[] mass = new int[n];
        int[] tempMass = new int[n];

        //Расчет
         for (int i = 0; i < n; i++) {
            for (int j = 1; j < n; j ++) {
                tempMass[i] = summScores(scores, i);
                }
            }

        //Сортировка пузырьком в обратном направлении
        for (int i = 0; i + 1 < n; i++) {
            for (int j = 0; j + 1 < n; j++) {
                if (tempMass[i + 1] > tempMass[j]) {
                    temp = tempMass[i + 1];
                    tempMass[i + 1] =  tempMass[j];
                    tempMass[j] = temp;
                }
            }
        }

        //вывод
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j ++) {
                if (tempMass[i] == summScores(scores, j)) {
                    //Долго дрочился как вывести число с округлением до десятых и при этом что бы чило не скакало +0.1
                    //при округлении, наверное можно было и Match вызвать((((
                    if (midScores(scores, j) % 1 == 0) {
                        System.out.print(name[j] + ": " + dogs[j] + ", " + midScores(scores, j));
                    } else {
                        System.out.print(name[j] + ": " + dogs[j] + ", "
                                + decimalFormat.format(midScores(scores, j)));
                    }
                }

            }
            System.out.println();
        }

    }

    public static int summScores (int scores[][], int inI) {
        int temp = 0;
        for (int j = 0; j < 3; j++) {
            temp += scores[inI][j];
        }
        return temp;
    }

    public static double midScores (int scores[][], int inI) {
        double temp = 0;
        for (int j = 0; j < 3; j++) {
            temp += scores[inI][j];
        }
        return temp / 3.0;
    }

}

/*
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
*/