//package homework2p2;

import java.util.Scanner;

/**
 * На вход подается число N — количество строк и столбцов матрицы. Затем
 * передаются координаты X и Y расположения коня на шахматной доске.
 * Необходимо заполнить матрицу размера NxN нулями, местоположение коня
 * отметить символом K, а позиции, которые он может бить, символом X.
 */

public class Solution3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        String[][] mass = new String[n][n]; //[кол-во_строк][кол-во_столбцов]

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == y && j == x) {
                    mass[i][j] = "K";
                }
                else {
                    if (Math.abs(i - y) == 2 && Math.abs(j - x) == 1 || Math.abs(i - y) == 1 && Math.abs(j - x) == 2) {
                    //if (Math.abs((Math.abs(i - y)) - (Math.abs(j - x))) == 1) {
                        //System.out.println((i - y) + " " + (j - x) + " i " + i + " j " + j);
                        mass[i][j] = "X";
                    } else {
                        mass[i][j] = "0";
                    }
                }
//;
            }
        }

        //Печать массива без пробела в конце;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j != n - 1) {
                    System.out.print(mass[i][j] + " ");
                } else {
                    System.out.print(mass[i][j]);
                }
            }
            System.out.println();
        }






    }
}
