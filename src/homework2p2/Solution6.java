package homework2p2;

import java.util.Scanner;

public class Solution6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] massEat = new int[4];
        int[][] massWeek = new int[7][4]; //[кол-во_строк][кол-во_столбцов]
        int summ = 0;
        boolean flag = false;

        //Заплоним массив 1
        for (int i = 0; i < 4; i++) {
           massEat[i] = scanner.nextInt();
        }

        //Заплоним массив 2
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                massWeek[i][j] = scanner.nextInt();
            }
        }

        //Печать массива без пробела в конце;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 7; j++) {
                summ += massWeek[j][i];
            }
            if (summ < massEat[i]) {
                flag = true;
                summ = 0;
            } else {
                flag = false;
                break;
            }
        }

        if (flag)
            System.out.print("Отлично");
        else System.out.print("Нужно есть поменьше");
    }
}

/**
882 595 1232 17500
116 85 76 2300
100 98 124 2500
182 70 154 2750
114 85 74 1900
96 77 60 1890
110 96 98 2500
155 67 124 2500

882 595 1232 17500
142 85 76 2300
100 93 124 2500
282 70 144 3350
114 85 74 1900
96 77 60 1890
110 96 98 2500
155 67 124 3790


 */
