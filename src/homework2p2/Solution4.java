//package homework2p2;

import java.util.Scanner;

/**
 * На вход подается число N — количество строк и столбцов матрицы. Затем
 * передается сама матрица, состоящая из натуральных чисел. После этого
 * передается натуральное число P.
 * Необходимо найти элемент P в матрице и удалить столбец и строку его
 * содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
 * Гарантируется, что искомый элемент единственный в массиве.
 */

public class Solution4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[][] mass = new int[n][n]; //[кол-во_строк][кол-во_столбцов]
        int x = 0;
        int y = 0;
        boolean printFlag = false;
        StringBuffer stringBuffer = new StringBuffer();

        //Заплоним массив
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
               mass[i][j] = scanner.nextInt();
            }
        }

        //Заданное число
        int p = scanner.nextInt();

        //Найдем координаты числа
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(mass[i][j] == p) {
                    x = i;
                    y = j;
                }
            }
        }


        //Печать массива без пробела в конце;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != x && j != y) {
                    printFlag = true;
                    stringBuffer.append(mass[i][j]).append(" ");
                }
            }

            if (printFlag) { // печатаем, только тогда, когда строка не опапала в исключение
                System.out.print(stringBuffer.substring(0, stringBuffer.length() - 1));
                stringBuffer.delete(0, stringBuffer.length());
                System.out.println();
                printFlag = false;
            }
        }
    }
}


/*   System.out.print(stringBuffer.substring(0, stringBuffer.length() - 1));
3
1 2 3
1 7 3
1 2 3
7

4
1 2 3 4
1 2 3 4
1 2 3 4
1 2 3 5
5


 */
