package oop1week2.oop1.task2;

import java.io.*;
import java.util.Scanner;

public class ReadFile {

    //Указать значение до файла

    private static final String FOLDER_DIRECTORY = "C:\\Users\\Diamond\\IdeaProjects\\untitled\\src\\week8\\oop1\\Task2\\file";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    //Утильный класс делается через privat
    private ReadFile() {

    }
    //исключение для записи и чтения файлов если их нет
    public static void readAndWriteData(String filePatch) throws IOException {
        Scanner scanner = new Scanner(new File(filePatch));
        //Scanner scanner = new Scanner(new File(FOLDER_DIRECTORY + "\\input.txt"));

        //Считатли инфу из фала
        String[] days = new String[10]; //Массив для хранения строк из файла
        int i = 0;
        while (scanner.hasNextLine()) { //пока файл не закончился
            days[i++] = scanner.nextLine();
        }

        //Запишим данные в файл
        Writer writer = new FileWriter(FOLDER_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String res = "Порядковый номер недели " + days[j] + " = " + WeekDays.ofName(days[j]).dayNumber + "\n";
            writer.write(res);
        }

        //Закрытие потоков чтения/записи
        writer.close();
        scanner.close();

        //Пример закрытия потоков try with resources (Closable интерфейс) -> не надо явно закрывать потоки (ресурсы)
        //try (Writer writer1 = new FileWriter("")) {
        //     System.out.println();
        // }
    }

    //перегруженный метод, если мы не знаем пути или имени файла
    public static void readAndWriteData() throws IOException {
        readAndWriteData(FOLDER_DIRECTORY + "\\input.txt");
    }
}
