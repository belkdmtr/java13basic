package oop1week2.oop1.task1;

//Метод переменный длины

public class VaribleLength {

    static int sum(int... numbers) {
        //int[] numbers //можно так вместо int...
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    static boolean findChar (Character ch, String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].indexOf(ch) != -1) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(sum(1, 2 ,3, 4, 5, 6));

        //System.out.println(findChar('a', "pyton", "java"));
        int a = 123;
        System.out.println(String.format("This is an integer: %d %d", a, 567));

    }
}

