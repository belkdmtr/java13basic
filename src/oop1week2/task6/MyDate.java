package oop1week2.task6;

import java.util.GregorianCalendar;

public class MyDate {

    private int yaear;
    private int month;
    private int day;

    public MyDate() {
        setDate(new GregorianCalendar().getTimeInMillis());
    }

    public MyDate(long elapsedTime) {
        setDate(elapsedTime);
    }

    //Безаргументный конструктор, который создает объект типа MyDate

    //Значения которые будут у нас поступать.
    public MyDate(int yaear, int month, int day) {

        this.yaear = yaear;
        this.month = month;
        this.day = day;
    }

    //Геттеры, то что мы хотим вернуть
    public int getYaear() {
        return yaear;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public void setDate(long elapsedTime) {
        GregorianCalendar date = new GregorianCalendar();
        date.setTimeInMillis(elapsedTime);

        yaear = date.get(GregorianCalendar.DAY_OF_YEAR);
        month = date.get(GregorianCalendar.MONTH);
        day = date.get(GregorianCalendar.DAY_OF_MONTH);

    }















}
