package javaProjects.oop1p2;

import java.util.Scanner;

public class Project {
    public static final double ROUBLES_PER_DOLLAR = 72.12; // курс доллара.
    public static void main(String[] args) {

        //int dollars; //Сумма денег в американских долларах. /*УДАЛЕНА В ХОДЕ ОБУЧЕНИЯ!
        //double roubles; //Сумма денег в российских рублях. /*УДАЛЕНА В ХОДЕ ОБУЧЕНИЯ!

        int[] dollarsArray;
        double[] roublesArray;

        //int digit; //Переменная для хранения последней цифры. /*УДАЛЕНА В ХОДЕ ОБУЧЕНИЯ!

        // Третья часть задачи.
        int n; //Количество конвертаций.
        int i; //Счетчик.

        Scanner input = new Scanner(System.in);

        //Отобразить инструкцию.
        instruct();

        //Получать количество конвертаций до тех пор,
        // пока не введено корректное значение

        do {
            System.out.print("Введите корректное количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);

        //Получить n сумм денег в американских долларах.
        System.out.print("Введите " + n + " сумму денег " +
                "в американских долларах через пробел: ");
        dollarsArray = new int[n];
        for (i = 0; i < n; i++) {
            dollarsArray[i] = input.nextInt();
        }

        //Конвертировать n сумм денег в российские рубли.
        roublesArray = find_roubles(dollarsArray, n);

        //Отобразить в таблице n сумм денег в американских долларах и
        //экивалентные им сыммы денег в российских рублях в пользу покупателя
        System.out.println("\n Сумма, $   Сумма, ₽");
        for (i = 0; i < n; i++) {
            System.out.println("\t" + dollarsArray[i] + "\t\t " +
                    (int)(roublesArray[i] * 100) / 100.0);
        }
    }

    //Отображает инструкцию к программе.
    public static void instruct () {
        System.out.println("Эта программа конвертирует сумму денег " +
                "из американских долларов в российские рубли.");
        System.out.println("Курc покупки равен " + ROUBLES_PER_DOLLAR + " рубля. \n");
    }

    /**
    //Конвертирует n сумм денег из американских долларов в российские рубли.
     */
    public static double[] find_roubles (int[] dollarsArray, int n) {
        double[] roubelsArray = new double[n];
        int i;

        for (i = 0; i < n; i++) {
            roubelsArray[i] = ROUBLES_PER_DOLLAR * dollarsArray[i];
        }
        return roubelsArray;
    }
}