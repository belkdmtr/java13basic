package homework3p3.Solution1;

//Супер класс млекопетающих
public class Mammal
        implements Animal {
    private String wayOfBirth = "живородящие животное";

    @Override
    public void born() {
        System.out.println("и " + wayOfBirth);
    }

    @Override
    public void move() {
    }

    @Override
    public void currentStatus(String food) {
    }
}
