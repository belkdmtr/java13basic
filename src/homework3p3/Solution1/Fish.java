package homework3p3.Solution1;

//Супер класс рыб
public class Fish
        implements Animal {
    private String wayOfBirth = "мечет икру";

    @Override
    public void born() {
        System.out.println("и " + wayOfBirth);
    }

    @Override
    public void move() {
    }

    @Override
    public void currentStatus(String food) {
    }
}
