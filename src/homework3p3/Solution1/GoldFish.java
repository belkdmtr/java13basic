package homework3p3.Solution1;

import java.util.random.RandomGenerator;

public class GoldFish
        extends Fish{

    private String speed = "медленно";
    private String typeOfMovement = "плавает";
    private String food;
    //Генерируем случайно, животное спит иои нет
    private boolean sleep = RandomGenerator.getDefault().nextBoolean();

    //Дадим имя методу читаемое (СИТЕМНАЯ ФИШКА!!!!)
    public String toString() {
        return "Золотая рыбка";
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getSpeed() {
        return speed;
    }

    public void setTypeOfMovement(String typeOfMovement) {
        this.typeOfMovement = typeOfMovement;
    }

    public String getTypeOfMovement() {
        return typeOfMovement;
    }

    //Выведим сведения о возможностях передвижения животного
    public void move () {
        System.out.print(this + " " + this.speed  + " " + this.typeOfMovement + " ");
        super.born();
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getFood() {
        return food;
    }

    //Выведим сведения о текущем статусе животного кушает оно или спит (оно не может одновремено и кушать и спать)
    public void currentStatus (String food) {
        if(this.sleep) {
            System.out.println("сейчас он спит и не может есть " + food);
        }
        else {
            System.out.println("сейчас он есть " + food);
        }
    }
}
