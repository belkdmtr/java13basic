package homework3p3.Solution1;

//Супер класс птиц
public class Bird
        implements Animal {
    private String wayOfBirth = "откладывает яйа";

    @Override
    public void born() {
        System.out.println("и " + wayOfBirth);
    }

    @Override
    public void move() {
    }

    @Override
    public void currentStatus(String food) {
    }
}
