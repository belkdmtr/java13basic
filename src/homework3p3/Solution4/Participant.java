package homework3p3.Solution4;

public class Participant {

    private String namePeople; //имя человека
    boolean reachTheFinal = false; // условие выхода в финал
    Dog dog = new Dog(); //закрепляем / создаем класс собаки за этим человеком

    public Participant(String namePeople) {
        this.namePeople = namePeople;
    }

    public String getNamePeople() {
        return namePeople;
    }

    public boolean isReachTheFinal() {
        return reachTheFinal;
    }

    public void setReachTheFinal(boolean reachTheFinal) {
        this.reachTheFinal = reachTheFinal;
    }

    @Override
    public String toString() {
        return namePeople;
    }
}
