package homework3p3.Solution4;

import java.text.DecimalFormat;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Создадим массив с участниками
        List<Participant> participants = new ArrayList<>();

        //Укажем количество участников
        int n = scanner.nextInt();
        //Маска для корректного отображения числа
        String pattern = "#0.0";

        //Заполним данные наших  участников
        for (int i = 0; i < n; i++) {
            participants.add(new Participant(scanner.next())); //Добавим имя человека
        }

        for (int i = 0; i < n; i++) {
            participants.get(i).dog.setNameDog(scanner.next()); //Добавим имя собаки
        }


        //Заполняем три оценки судей наших участников
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                participants.get(i).dog.setScore(scanner.nextInt());
            }
        }


        //Выведим первые лучшие три команды
        for (int i = 0; i < 3; i ++) {
            int totalParticipants = 0;//переменная для хранения выбранной лучшей команды в каждой из итераций
            for (int j = 1; j < n; j++) {
                //Здесь сначала выбирается самая сильная команда и дальше на уменьшение....
                //Если команда не вышла в финал && сравниваем ее с другими командами и
                // если команда [j - 1] хуже команды [j]  - сохраняем индекс выбранной команды и
                if (!participants.get(j).isReachTheFinal()
                        && participants.get(j - 1).dog.getTotalScore() < participants.get(j).dog.getTotalScore()) {
                    totalParticipants = j;
                }
            }
            //присваиваем true выбранной команде, что она вышла в финал
            participants.get(totalParticipants).setReachTheFinal(true);
            //Далее выводим результат
            System.out.println(participants.get(totalParticipants).getNamePeople() + " " + participants.get(totalParticipants).dog.getNameDog() +
                    " " + (int)(participants.get(totalParticipants).dog.getTotalScore() * 10) / 10.0);
        }
    }
}



/*
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
*/