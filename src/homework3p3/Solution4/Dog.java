package homework3p3.Solution4;

public class Dog {

    private String nameDog;
    private int score = 0; //счетчик оценок
    private double totalScore; //конечный результат оценок (три оценки поделить на три)

    public void setNameDog(String nameDog) {
        this.nameDog = nameDog;
    }

    public String getNameDog() {
        return nameDog;
    }

    //Оценка собаки, складывается
    public void setScore(int score) {
       this.score += score;
    }

    public double getTotalScore() {
        totalScore();
        return totalScore;
    }

    //Метод для подсчета средней оценки.
    private void totalScore () {
      this.totalScore = score / 3.0;
    }

    public String toString() {
        return nameDog;
    }

}
