package homework3p3.Solution2;

/**
 * Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
 * сожалению, из Мебели он умеет чинить только Табуретки, а Столы, например,
 * нет. Реализовать метод в цеху, позволяющий по переданной мебели
 * определять, сможет ли ей починить или нет. Возвращать результат типа
 * boolean. Протестировать метод.
 */

public class Main {
    public static void main(String[] args) {

        //Создаем объект стул
        Object seat = new Seat();
        //Создаем объект сто
        Object table = new Table();

        //Проверка
        System.out.println("Можно починить " + seat.toString() + "? " + BestCarpenterEver.checkTheProduct(seat));
        System.out.println("Можно починить " + table.toString() + "? " + BestCarpenterEver.checkTheProduct(table));
    }
}
