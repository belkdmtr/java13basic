package homework3p3.Solution2;

public class BestCarpenterEver {

    //Создаем без аргументов конструктор, что бы не было ошибки
    //К тому же это утильный класс
    protected BestCarpenterEver() {
    }

    //Проверяем, является ли переданные продукт из класса Main стулом?
    public static boolean checkTheProduct(Object product) {
        if (product instanceof Seat) {
            return true; //если объект стул, то чиним
        } else {
            return false; //если другой обект, то не чиним
        }
    }
}
