package homework3p3.Solution3;

//Клас образующий колонки двумерного массива | 1 | 2 | n..... |
//т.е. это одна из ячеек массива
public class Colums {
    //переменная хранязщая значение ячейки нашего массива
    private Integer colums;

    public Colums(Integer colums) {
        this.colums = colums;
    }

    public Integer getColums() {
        return colums;
    }
}