package homework3p3.Solution3;

/**
 * На вход передается N — количество столбцов в двумерном массиве и M —
 * количество строк. Необходимо вывести матрицу на экран, каждый элемент
 * которого состоит из суммы индекса столбца и строки этого же элемента. Решить
 * необходимо используя ArrayList.
 * Входные данные 2 2       Выходные данные 0 1
 *                                          1 2
 *
 * Входные данные 3 5       Выходные данные 0 1 2
 *                                          1 2 3
 *                                          2 3 4
 *                                          3 4 5
 *                                          4 5 6
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Integer m; //Количество строк
        Integer n; //Количество стобцов

        //Объявление двумерного массива ArrayList, который будет в себе хранить еще один масмсив:
        //Сперва объявляем массив строк он же строка
        ArrayList<Colums> rows = new ArrayList<>();
        Colums colums = null;

        //columns - колонки
        // rows - строки

        System.out.print("Введите через пробел размерность массива [столбцов][строк]: ");
        n = scanner.nextInt();
        m = scanner.nextInt();

        //Вставка значений в двумерный ArrayList:
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                colums = new Colums(i + j);
                rows.add(colums);
            }
        }

        //Вывод двумерного массива на экран:
        //arrStartIndex переменная нужна, что бы разграничить итерации цикла на строки = n,
        //так как массив ArrayList лист у нас строка [[0][1]] [[2][3]] ........
        //         или
        // [[Colums 0][Colums 1]]
        // [[Colums 2][Colums 3]]
        // [[Colums n][Colums n]]

        int arrStartIndex = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0 + arrStartIndex; j < n + arrStartIndex; j++) {
                //получаем значение записанное в класс Colums, который хранится в 0 ячейке ArrayList
                System.out.print(rows.get(j).getColums() + " ");
            }
            arrStartIndex += n; //следующая итерация у нас уже начнется со второй строки массива
            System.out.println();
        }
    }
}