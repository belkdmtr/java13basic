package week7.oop1.task1;

//ВКД ВЫКЛ ЛЮСТРУ
//В ЛЮСТРЕ МНОГО ЛАМП

public class Chandelier {
    private Bulb[] chandelier;

    //Конструктор люстры - передаем количсество ламп м срздаем объхекты ламп в массивее
    public Chandelier(int countOfBulbs) {
        chandelier = new Bulb[countOfBulbs]; //массив

        for(int i = 0; i < countOfBulbs; i++) {
            chandelier[i] = new Bulb(); //в цикле создаем объекты ламп
        }
    }

    public void turnOn() {
        for (Bulb bulb: chandelier) {
            bulb.turnOn();
        }
    }

    public void turnOff() {
        for (Bulb bulb: chandelier) {
            bulb.turnOff();
        }
    }

    //Логика выполнения
    public boolean isSining() {
        return chandelier[0].isShining();
    }










}
