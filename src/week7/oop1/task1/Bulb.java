package week7.oop1.task1;


 /*
 * Реализовать класс “Лампа”. Методы:
 * включить лампу
 * выключить лампу
 * получить текущее состояние
 *  */

public class Bulb {
    private boolean toggle;

    //Конструктор по умолчанию
    public Bulb() {

        this.toggle = false; //Инициализируем переменную
    }

    public Bulb(boolean toggle) { //Пример перегруженного метода

        this.toggle = toggle;
    }

    //Методы включения выключения лампы
    public void turnOn() {

        this.toggle = true;
    }

    public void turnOff() {

        this.toggle = false;
    }

    //Метод получения состояния переменной
    public boolean isShining() {

        return this.toggle;
    }



}
