package week7.oop1.task1;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb(); //Конструктор без аргументов всегда существует и делает новый экзмепляр класса
        System.out.println("Светит ли лампа сейча? - " + bulb.isShining());

        System.out.println("ВКЛ. лампу! ");
        bulb.turnOn();
        System.out.println("Светит ли лампа сейча? - " + bulb.isShining());

        System.out.println("ВЫКЛ. лампу! ");
        bulb.turnOff();
        System.out.println("Светит ли лампа сейча? - " + bulb.isShining());

        //Люстра
        Chandelier chandelier = new Chandelier(4);
        chandelier.turnOn();
        chandelier.turnOff();

    }
}
