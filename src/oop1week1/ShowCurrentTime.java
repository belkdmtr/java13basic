package oop1week1;

/**
 * Для создания экземпляра текущей даты и времени можно использовать
 * безаргументный конструктор класса Date, для возврата времени
 * в миллисекундах, прошедшего с 1 января 1970 г., - метод getTime(),
 * а для возврата даты и времени в виде строки — метод toString().
 * Например, следующий код
 */

public class ShowCurrentTime {
    public static void main(String[] args) {
        java.util.Date date = new java.util.Date();
        System.out.println("Время, прошедшее с 1 января 1970 г., равно " +
                date.getTime() + " миллисекунд.");
        System.out.println(date.toString());
    }
}

/**
 * У класса Date есть еще один конструктор Date(long elapseTime),
 * который можно использовать, чтобы создать объект типа Date для
 * заданного времени в миллисекундах, прошедшего с 1 января 1970 г., GMT.
 */
