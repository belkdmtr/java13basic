package oop1week1;

/**
 * Задание №3: Класс Date
 * Напишите программу, которая создает объект Date, устанавливает у него прошедшее время,
 * равным 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000 и 100000000000,
 * и отображает дату и время с помощью метода toString(), соответственно.
 */

public class Task2to1Time {
    public static void main(String[] args) {
        java.util.Date date = new java.util.Date();
        date.setTime(10000);
        System.out.println(date.toString());

        date.setTime(100000);
        System.out.println(date.toString());

        date.setTime(1000000);
        System.out.println(date.toString());

        date.setTime(10000000);
        System.out.println(date.toString());

        date.setTime(100000000);
        System.out.println(date.toString());

        date.setTime(1000000000);
        System.out.println(date.toString());

        date.setTime(10000000000L);
        System.out.println(date.toString());

        date.setTime(100000000000L);
        System.out.println(date.toString());
    }



/*
    // Определение класса Date
    class Date {
        //переменная с прошедшим временем
        long lastTime;

        //Показывает дату с учетом прошедшего времени

    }
*/
}
