package oop1week1;

/**
 * При создании объекта типа Random необходимо указывать начальное
 * число или использовать начальное число, заданное по умолчанию.
 * Начальное (случайное) число — это число, которое используется для
 * инициализации генератора псевдослучайных чисел. Безаргументный
 * конструктор создает объект типа Random, используя текущее прошедшее
 * время в качестве его начального значения. Если два объекта типа Random
 * имеют одно и то же начальное число, то они генерируют одинаковые последовательности чисел.
 * Например, следующий код создает два объекта типа Random с одним и тем же начальным числом, равным 3:
 */

import java.util.Random;

public class RandomObject {
    public static void main(String[] args) {
        Random generator1 = new Random(3);
        System.out.print("Из generator1: ");
        for (int i = 0; i < 10; i++)
            System.out.print(generator1.nextInt(1000) + " ");
        Random generator2 = new Random(3);
        System.out.print("\nИз generator2: ");
        for (int i = 0; i < 10; i++)
            System.out.print(generator2.nextInt(1000) + " ");

        Random generator3 = new Random(4);
        System.out.print("\nИз generator3: ");
        System.out.print(generator3.nextInt(1000) + " ");

    }
}

/**
 * Этот код генерирует одну и ту же последовательность случайных значений типа int:
 *
 * Из generator1: 734 660 210 581 128 202 549 564 459 961
 *
 * Из generator2: 734 660 210 581 128 202 549 564 459 961
 */
