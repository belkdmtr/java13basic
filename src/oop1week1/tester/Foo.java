package oop1week1.tester;

public class Foo {
    int i;
    static int s;

    public static void main(String[] args) {
        Foo f1 = new Foo();
        System.out.println("f1.i равно " + f1.i + " f1.s равно " + f1.s);
        Foo f2 = new Foo();
        System.out.println("f2.i равно " + f2.i + " f2.s равно " + f2.s);
        Foo f3 = new Foo();
        System.out.println("f3.i равно " + f3.i + " f3.s равно " + f3.s);
    }

    public Foo() {
        i++;
        s++;
    }
}