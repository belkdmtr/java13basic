package oop1week1.tester;

class Test {
    private double i;

    public Test(double i) {
        t();
        i = i;
    }

    public Test() {
        this(1);
        System.out.println("Заданный по умолчанию конструктор");
    }

    public void t() {
        System.out.println("Вызов метода t()");
    }

    public static void main(String[] args) {
        Test test = new Test(1);
        System.out.println(test);
    }

}
