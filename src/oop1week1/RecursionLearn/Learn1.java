package oop1week1.RecursionLearn;

//Программа ComputeFactorial предлагает пользователю ввести
// неотрицательное целое число, а отображает факториал этого числа.

import java.util.Scanner;

public class Learn1 {
    /**
     * Метод main
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите неотрицательное целое число: ");
        int n = input.nextInt();

        // Отобразить n!
        System.out.println(n + "! равно " + factorial(n));
    }

    /**
     * Возвращает n!
     */
    public static long factorial(int n) {
        if (n == 0) {// простой случай
            //System.out.println("N = 0");
            return 1;
        }
        else
            //System.out.println(n);
            return n * factorial(n - 1); // рекурсивный вызов
    }
}
