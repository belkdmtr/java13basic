package oop1week1.RecursionLearn;

public class Learn6 {
    public static void main(String[] args) {
        xMethod(1234567);
    }
    public static void xMethod(int n) {
        if (n != 1) {
            System.out.print(n);
            xMethod(n / 10);
        }
    }
}
