package oop1week1.RecursionLearn;

public class Learn4 {
    public static void main(String[] args) {
        System.out.println( power_raiser(2, 9));

    }
    public static int power_raiser(int base, int power) {
        if (power == 0)
        return (1);
  else
        return (base * power_raiser(base, power - 1));
    }

}
