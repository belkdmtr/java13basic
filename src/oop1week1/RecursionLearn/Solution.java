package oop1week1.RecursionLearn;

//Маскировка номера карты

//Напишите программу, которая маскирует номер банковской карты таким образом,
// чтобы в консоли отображались только четыре последних символа, а все
// остальные были заменены на символ *. Если входная строка содержит
// не больше четырех символов, то вернуть входную строку.

public class Solution {
    public static void main(String[] args) {
        String cardNumber = "2202203850046002";

        String updatedCardNumber = maskCharacters(cardNumber, 4);

        // Вывести маскированный номер карты
        System.out.println("Маскированный номер банковской карты:" + updatedCardNumber);

    }

    /**
     * Замена символов строки {@code input} символом '*'
     * с начала строки, не менять в конце {@code numberFromEnd} символа(ов)
     */
    static String maskCharacters(String input, int numberFromEnd) {

        // Если количество отражаемых в конце символов меньше нуля
        // или больше или равно длине строки
        if (numberFromEnd < 0 || numberFromEnd >= input.length()) {
            // вернуть исходную строку
            return input;
        }

        // заменяем первый элемент строки
        // и делаем рекурсивный вызов для подстроки со 2-ого символа
        return "*" + maskCharacters(input.substring(1), numberFromEnd);


    }



}
