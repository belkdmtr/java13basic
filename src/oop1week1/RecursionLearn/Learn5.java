package oop1week1.RecursionLearn;

public class Learn5 {
    public static void main(String[] args) {
        System.out.println(recurs(6));
    }

    public static long recurs(int n) {
        if (n == 1) {// простой случай
            return 1;
        } else
            return (n + recurs(n - 1)); // рекурсивный вызов
    }

}
