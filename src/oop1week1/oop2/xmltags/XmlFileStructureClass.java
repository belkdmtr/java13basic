package oop1week1.oop2.xmltags;

//<HEADER XML VERSION=0.1>
//    <FirstTag Тест1=12345235>hello java</FirstTag>
//    <SecondTag>hello java 2 </SecondTag>
//    <NTag>hello_java_N</NTag>
//</HEADER>
public abstract class XmlFileStructureClass {
    protected final DataSource dataSource;
    
    public XmlFileStructureClass(DataSource dataSource) {
        this.dataSource = dataSource;
        fillHeadOfFile();
        
    }
    
    protected void fillHeadOfFile() {
        //заполнять самый верхний тэг xml файла
        // (так как он всегда статический и не изменяем для всех)
        fillFirstTag();
        fillSecondTag();
        fillNTag();
    }
    
    abstract void fillFirstTag();
    
    abstract void fillSecondTag();
    
    abstract void fillNTag();
}
