package oop1week1.oop2.interfacce;

public class Wolf
        implements Animal{
    @Override
    public String getColor() {
        return "серый";
    }

    @Override
    public String getVoice() {
        return "воет";
    }

    @Override
    public void eat(String food) {
        System.out.println("Еда для волка - только дикие звери");
    }
}
