package oop1week1.TestCircle;

public class Circle {
    /**
     * Радиус круга
     */
    private double radius; //инкапсуляция

    /**
     * Количество созданных объектов
     */
    private static int numberOfObjects = 0;

    /**
     * Создает круг с радиусом, равным 1
     */
    public Circle() {
        radius = 1.0;
        numberOfObjects++;
    }

    /**
     * Создает круг с указанным радиусом
     */
    public Circle(double newRadius) {
        radius = newRadius;
        numberOfObjects++;
    }

    /** Возвращает радиус */ //------------------------ getter
    public double getRadius() {
        return radius;
    }

    /**
     *  Если новый радиус отрицательный, то объекту в качестве радиуса присваивается 0.
     *  Присваивает новый радиус   ----------------------- setter
     */
    public void setRadius(double newRadius) {
        radius = (newRadius >= 0) ? newRadius : 0;
    }

    /**
     * Возвращает количество созданных объектов
     */
    public static int getNumberOfObjects() {

        return numberOfObjects;
    }

    /**
     * Возвращает площадь круга
     */
    public double getArea() {
        return radius * radius * Math.PI;
    }
}
