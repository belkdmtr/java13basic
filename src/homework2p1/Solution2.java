package homework2p1;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого аналогично передается второй
 * массив (aj) длины M.
 * Необходимо вывести на экран true, если два массива одинаковы (то есть
 * содержат одинаковое количество элементов и для каждого i == j элемент ai ==
 * aj). Иначе вывести false.
 * Ограничения:
 * ● 0 < N < 100
 * ● 0 < ai < 1000
 * ● 0 < M < 100
 * ● 0 < aj < 1000
 * Пример:
 * Входные данные                   Выходные данные
 * 71
 * 2 3 4 5 6 7
 *                                  true
 *
 * 71
 * 2 3 4 5 6 7
 * 3 89 12 46
 * 3 12 89 46
 *                                  false
 * 1 15
 * 42
 * 4 6 8
 *                                  false
 */

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] ai = new int[n];

        boolean res = false;

        //Заполним массив ai
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();
        int[] aj = new int[m];

        //Заполним массив aj
        for (int j = 0; j < n; j++) {
            aj[j] = scanner.nextInt();
        }

        if (ai.length == aj.length) {
            for (int i = 0; i < n; i++) {
                if (ai[i] == aj[i]) {
                   res = true;
                }
                else {
                    break;
                }
            }
        }

        System.out.println(res);

    }
}
