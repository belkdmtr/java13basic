package homework2p1;

import java.util.Arrays;
import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо создать массив, полученный из исходного возведением в квадрат
 * каждого элемента, упорядочить элементы по возрастанию и вывести их на
 * экран.
 * Ограничения:
 * ● 0 < N < 100
 * ● -1000 < ai < 1000
 */

public class Solution7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int res = 0;

        int[] ai = new int[n];

        //Заполним массив
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }

        //Возведем каждый элемент в квадрат
        for (int i = 0; i < ai.length; i++) {
            res = (int) Math.pow(ai[i], 2);
            ai[i] = res;
        }

        Arrays.sort(ai);
        String str = "";

        for (int i = 0; i < ai.length; i++) {
            str += ai[i] + " ";

        }

        //Вывод результата без пробела в конце
        System.out.print(str.substring(0, str.length() - 1));




    }
}

//-10 -5 1 3 3 8
