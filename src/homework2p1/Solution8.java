package homework2p1;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого передается число M.
 * Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
 * для которого |ai - M| минимальное). Если их несколько, то вывести
 * максимальное число.
 *
 * Входные данные
 * 6
 * -10 9 -5 -6 1 -3
 * -4
 *
 * Выходные данные -3
 *
 * **********************
 *
 *  Входные данные
 *  2
 *  10 20
 *  21
 *
 *  Выходные данные 20
 *
 */

public class Solution8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(); //Длина массива
        int[] ai = new int[n];

        //Заполним массив
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }

        int m = scanner.nextInt(); //Заданное значение М

        //Массивы для хранения временных минимальных и максимальных значений
        int[] minValue = new int[n];
        int[] maxValue = new int[n];

        //Сначала найдем минимальное значение
        //Заполним массив значениями, которые меньше М
        for (int i = 0; i < n; i++) {
            if (ai[i] < m)
                minValue[i] = ai[i];
        }

        //Теперь найдем Максимальное значения из выбранных минимальных (оно и будет ближе к М)
        int min = minValue[0];
        for (int i = 1; i < n; i++) {
            if (minValue[i] != 0) {
                min = Math.max(min, minValue[i]);
            }
        }

        //Теперь найдем максимально билзкое значение
        //Заполним массив значениями, которые больше М
        for (int i = 0; i < n; i++) {
            if (ai[i] > m)
                maxValue[i] = ai[i];
        }

        //Теперь найдем минимальное значение из выбранных максимальных (оно и будет ближе к М)
        int max = maxValue[0];
        for (int i = 1; i < n; i++) {
            if (maxValue[i] != 0) {
                max = Math.min(max, maxValue[i]);
            }
        }

        //Выведим максимальное значение из выбранных
        System.out.println(Math.max(min, max));

    }
}

//-10 9 -5 -6 1 -3