package homework2p1.practicle.forweekpracticle;

/*
Дано число n < 13, n > 0. Найти факториал числа n (n! = 1 * 2 * 3 * … * (n - 1) * n)
7 -> 5040

//n < 13 для того чтобы не выйти за пределы диапазона int

Отличие в том, что если знаки перед буквой, то сначала она наращивается на 1,
а потом только используется, а если знаки слева,
то сначала используется, а потом наращивается

 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int res = 1;

        for (int i = 2; i <= n; i++) {
            res *= i;
            System.out.println("Промежуточныйц результат " + res);
        }
        System.out.println("Итоговый результат " + res);
    }
}
