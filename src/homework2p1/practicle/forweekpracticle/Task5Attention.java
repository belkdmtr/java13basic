package homework2p1.practicle.forweekpracticle;

/*
Дано целое число n, n > 0. Вывести сумму всех цифр этого числа.
92180 -> 20 //9 + 2 + 1 + 8 + 0 == 20
 */


import java.util.Scanner;

public class Task5Attention {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int sum = 0;

        String s = ""+n;

        int max = s.length();

        for (int i = 1; i <= max; i++) {
            sum += (s.charAt(i-1) - '0');
        }
        System.out.println(sum);
    }
}
