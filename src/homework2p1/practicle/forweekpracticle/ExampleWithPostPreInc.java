package homework2p1.practicle.forweekpracticle;

public class ExampleWithPostPreInc {
    public static void main(String[] args) {
        //post-inc
        int i = 1;
        int a = i++; //сначала присваивается потом увеличивается ++

        int temp = i;
        i = i + 1;
        a = temp;

        System.out.println(i);
        System.out.println(a);

        //pre-inc

        int j = 1;
        int b = ++i;

        System.out.println(j);
        System.out.println(b);

    }
}
