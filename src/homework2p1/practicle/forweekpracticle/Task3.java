package homework2p1.practicle.forweekpracticle;

/*
Даны числа m < 13 и n < 7.
Вывести все степени (от 0 до n включительно) числа m с помощью цикла.
3 6
->
1
3
9
27
81
243
729
 */


import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        int res = 1;
        System.out.println(res); //К решению 2;

        for (int i = 0; i <= n; i++) {
            System.out.println((int)Math.pow(m, i)); //Решение № 1
           // res = res * m;
           // System.out.println(res);
        }
    }
}

