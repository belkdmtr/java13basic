package homework2p1.practicle.fiveweekpracticle;

/*
  На вход подается число N - длина массива.
     Затем передается массив строк длины N.
     После этого - число M.

     Сохранить в другом массиве только те элементы, длина строки которых
     не превышает M.

     Входные данные:
     5
     Hello
     good
     to
     see
     you
     4
Hello good to see you 4
     Выходные данные:
     good to see you
 */




import java.util.Scanner;

public class Task6 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            int m = scanner.nextInt();

            int flag = 0;

           String[] arr1 = new String[n];
           String[] arr2 = new String[n];


            for (int i = 0; i < n; i++) {
                arr1[i] = scanner.next();
                if (arr1[i].length() < m) {
                   arr2[i] = arr1[i];
                }
            }

            System.out.println(java.util.Arrays.toString(arr2));


           }
    }


