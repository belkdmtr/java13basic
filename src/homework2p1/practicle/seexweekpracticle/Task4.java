package homework2p1.practicle.seexweekpracticle;


/*
Развернуть строку рекурсивно.

abcde -> edcba
 */


import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        System.out.println(reverseString(s));

    }

    public static String reverseString(String s) {
        if (s.isEmpty()) {
            return s;
        }
       else {
            return reverseString(s.substring(1) + s.charAt(0)) ;
            //StringBuilder StringBuilder = new StringBuilder();
            //StringBuilder.reverse();
        }
    }
}
