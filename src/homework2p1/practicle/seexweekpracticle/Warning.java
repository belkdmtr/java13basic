package homework2p1.practicle.seexweekpracticle;

/*
//** НА СОБЕСЕДОВАНИЕ!!!!!!!!!!!!!!!!
   Написать функцию через рекурсию для вычисления суммы заданных положительных целых чисел a b
   без прямого использования оператора +.
    */


import java.util.Scanner;

public class Warning {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(sum(a, b));

    }

    public static int sum (int a, int b) {
        //int temp = a;
        if (b == 0) {
            return a;
        } else {
            return sum(a + 1 , b - 1);
        }
    }
}
