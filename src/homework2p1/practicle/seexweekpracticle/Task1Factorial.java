package homework2p1.practicle.seexweekpracticle;

//Для собеседования!!!!!!!!!!

/*
Найдем факториал числа n рекурсивно.
 */


import java.util.Scanner;

public class Task1Factorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int res = factorial(n);
        System.out.println("Факториал равен: " + res);

    }
    public static int factorial(int n) {
        if (n <= 1) { //факториал от 0 до 1 равно 1
            return 1;
        }
        return n * factorial(n - 1); // рекурсивный вызов
    }
}
