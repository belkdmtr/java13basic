package homework2p1.practicle.seexweekpracticle;

import java.util.Scanner;

/*
   Написать функцию через рекурсию для вычисления суммы заданных положительных целых чисел a b
   без прямого использования оператора +.
    */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println("Summa: " + sum(a, b));
    }

    private static int sum(int a, int b) {
        if (b == 0) {
            return a;
        }
        return sum(a + 1, b - 1);
    }
}