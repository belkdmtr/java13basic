package homework2p1.practicle.firstweekpracticle;
/*
     Даны целые числа a, b и с, определяющие квадратное уравнение. Вычислить дискриминант.

     Подсказка: D = b^2 - 4 * a * c

     Входные данные
     a = 6 b = -28 с = 79
     */

public class Task9 {
    public static void main(String[] args) {
        double a = 6;
        double b = -28;
        double c = 79;
        double d;

        d = Math.pow(b, 2) - 4 * a *c;
        System.out.println(d);


    }


}

