package homework2p1.practicle.firstweekpracticle;

/*
    Даны числа a, b, c. Нужно перенести значения из a -> b, из b -> с, и из c -> а.

    Входные данные:
    a = 3, b = 2, c = 1.
     */

import java.util.Scanner;

public class Task1 {
    //точка входа в программу
    public static void main(String[] args) {
        Scanner scaner = new Scanner(System.in);
        int a = scaner.nextInt();
        int b = scaner.nextInt();
        int c = scaner.nextInt();

        System.out.println("Результат ввода а=" + a + ";b=" + b + ";с=" + c);
        int temp = c;
        c = b;
        b = a;
        a = temp;

        System.out.println("Результат работы проги а=" + a + ";b=" + b + ";с=" + c);
        System.out.printf("a = %s, b = %s, c = %s", a, b ,c);



    }

}
