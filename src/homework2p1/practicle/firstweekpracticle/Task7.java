package homework2p1.practicle.firstweekpracticle;

/*
     Перевод литров в галлоны. С консоли считывается число n - количество литров, нужно перевести это число в галлоны.
     (1 литр = 0,219969 галлона)
     */

import java.util.Scanner;

public class Task7 {
    public static final double LITERS_IN_GALLON = 0.219969; //объявленная константа

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double res = n * LITERS_IN_GALLON;
        System.out.println("Результат: " + res);

    }
}
