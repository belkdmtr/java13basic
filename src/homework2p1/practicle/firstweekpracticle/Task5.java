package homework2p1.practicle.firstweekpracticle;

import java.util.Scanner;

/*
        Напишите аналог функции swap, которая меняет значения двух параметров местами (без вспомогательной переменной)
        Входные данные
        a = 8; b = 10
     */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = 8;
        int b = 10;

        System.out.println("Входные данные: a=" + a + " b=" + b);

        a = a + b; //8 + 10 = 18 --- a = 18
        b = a - b; //18 - 10 = 8 --- b = 8
        a = a - b; //18 - 8 = 5 --- a = 10

        System.out.println("Выходные данные: a=" + a + " b=" + b);


    }
}
