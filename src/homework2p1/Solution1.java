package homework2p1;

//1. (1 балл) На вход подается число N — длина массива. Затем передается массив
//вещественных чисел (ai) из N элементов.
//Необходимо реализовать метод, который принимает на вход полученный
//массив и возвращает среднее арифметическое всех чисел массива.
//Вывести среднее арифметическое на экран.

//Ограничения:
//● 0 < N < 100
//● 0 < ai < 1000

//Пример:
//Входные данные    Выходные данные
//3 1.5 2.7 3.14    2.4466666666666668
//2 30.42 12        21.21

import java.util.Scanner;

public class Solution1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double[] ai = new double[n];
        double res = 0;

        //Заполним массив
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextDouble();
        }

        for (int i = 0; i < n; i++) {
            res += ai[i];
        }

        System.out.println(res/n);

    }
}
