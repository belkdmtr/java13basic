package homework2p1;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо вывести на экран построчно сколько встретилось различных
 * элементов. Каждая строка должна содержать количество элементов и сам
 * элемент через пробел.
 * Java 12 Базовый модуль Неделя 4
 * ДЗ 2 Часть 1
 * Ограничения:
 * ● 0 < N < 100
 * ● -1000 < ai < 1000
 * Пример:
 * Входные данные Выходные данные
 * 67
 * 7 7 10 26 26
 *                      3 7
 *                      1 10
 *                      2 26
 * 2 -
 * 5
 * 7
 *                      1 -5
 *                      1 7
 */

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] ai = new int[n];

        //Заполним массив ai
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }

        for (int i = 0; i < n; i++) {
            i = CountMass(ai, i);
        }
    }

    public static int CountMass(int[] arr, int iIndex) {
        int count = 1;
        int temp = 0;
        int temp2 = 0;
        int i = 0;
        for (i = iIndex; i < arr.length - 1; i++) {
            if (arr[i] == arr[i + 1] && i + 1 < arr.length) {
                count++;
            }
            else {
                break;
            }
        }

        temp = arr[i];
        temp2 = i;

        System.out.println(count + " " + temp);
        return temp2;
    }
}