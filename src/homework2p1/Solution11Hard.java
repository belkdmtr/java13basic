package homework2p1;

import java.util.*;

public class Solution11Hard {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = -1;
        Boolean flag = true;

        System.out.print("Введите дилну пароля N: ");

        /**
         * Проверка условий пароля
         * что N >= 8, иначе вывести на экран "Пароль с N количеством символов
         * небезопасен" (подставить вместо N число) и предложить пользователю еще раз
         * ввести число N.
         */
        while (flag) {

            n = scanner.nextInt();

            if (n < 0)
                flag = false;
            if (n < 8) {
                System.out.println("Пароль c " + n + " количеством символов не безопасен");
                System.out.print("Попробуйте еще раз вести длину пароля: ");
            } else {
                //passGen(n); //вызываем метод, который генерит пароль и возращает строку с
                //В общем вызываем сначала passGen(n), который генерит и возращает строку password с паролем
                //а потом  вызываем миксер MixChart(passGen(n), n), который мешает символы в пароле и
                //печатает их
                System.out.print("Ваш пароль сгенерирован: ");
                MixChart(passGen(n), n); //
                flag = false; // условие выполнения программы
            }
        }
    }


    //Метод генерации паролей
    public static String passGen (int n) {

        int count = n / 4; //находим целое количество символов
        String password = "";

        //генерируем маденькие буквы a-z;
        for (int i = 0; i < count; i++) {
            password += String.valueOf((char) ((Math.random() * 26) + 97)); //генерируем случайную букву до 122 - от 97 = 26 от a-z
        }

        //генерируем большие буквы A-Z;
        for (int i = 0; i < count; i++) {
            password += String.valueOf((char) ((Math.random() * 26) + 65)); //генерируем случайную букву до 90 - от 65 = 26 от a-z
        }

        //генерируем цифры от 0 до 9
        for (int i = 0; i < count; i++) {
            password += (int)(Math.random() * 10);
        }

        //генерируем трез знаков _ * -
        for (int i = 0; i < count; i++) {
            switch ((int)(Math.random() * 3)) { // диапазон от 0 до 2х включительно (не включая 3)
                case 0 -> password += "_";
                case 1 -> password += "*";
                case 2 -> password += "-";
            }
        }

        //дополнительный генератор, если n / 4 имеет остаток, тогда тупо забиваем остаток цифрами.
        if (n % 4 != 0) {
            count = n % 4;
            //генерируем цифры от 0 до 9
            for (int i = 0; i < count; i++) {
                password += (int)(Math.random() * 10);
            }
        }
        return password;

    } //Конец метода


    //Метод перемешки строки с паролем
    public static void MixChart(String strPasVector, int n) {
        String[] arr = new String[n];

        //Заполним массив из строки
        for (int i = 0; i < n; i ++) {
            arr[i] = String.valueOf(strPasVector.charAt(i));
        }

        //Перемешаем массив
        Random random = new Random();
        for (int i = 0; i < arr.length; i ++) {
            int randomIndex = random.nextInt(arr.length); //выберим рандомное число от 1 до длины пароля
            String temp = arr[randomIndex];
            arr[randomIndex] = arr[i];
            arr[i] = temp;
        }

        //Напечатаем пароль.
        for (int i = 0; i < n; i ++) {
            System.out.print(arr[i]);
        }
    }
}
