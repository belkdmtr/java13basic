package homework2p1;

import java.util.Arrays;
import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо создать массив, полученный из исходного возведением в квадрат
 * каждого элемента, упорядочить элементы по возрастанию и вывести их на
 * экран.
 * Ограничения:
 * ● 0 < N < 100
 * ● -1000 < ai < 1000
 */

public class Solution12Hard {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] ai = new int[n];

        //Заполним массив
        for (int i = 0; i < n; i++) {
            ai[i] = (int)Math.pow(scanner.nextInt(), 2);
        }

        //возводим в степень элементы массива
        //masPow(ai, n);

        //СОРТИРОВКА ВЫБОРОМ сложность О(n)
        for (int i = 0; i < n; i++) {
            int min = ai[i]; //загружаем минимальное в минимальную переменную уже число возведенное в степень
            int minIndx = i;
            for (int j = i + 1; j < n; j++) {
                if (ai[j] < min) { //сравниваем последующий с предыдущим
                    min = ai[j];
                    minIndx = j;
                }
            }
            //Внешний цикл замены цифр
            int temp = ai[i];
            ai[i] = min;
            ai[minIndx] = temp;

            //Вывод результата без пробела в конце
            if (i != n - 1) {
                System.out.print(ai[i] + " ");
            } else {
                System.out.print(ai[i]);
            }
        }

        //Сортировка пузырьком в прямом направлении
       /* for (int i = 0; i + 1 < n; i++) {
            for (int j = 0; j + 1 < n; j++) {
                if ((int)Math.pow(ai[i + 1], 2) < (int)Math.pow(ai[j], 2)) {
                    temp = ai[i + 1];
                    ai[i + 1] =  ai[j];
                    ai[j] = temp;
                }
            }
        }*/
    }
}

/*
6
-10 -5 1 3 3 8

 */
//1 9 9 25 64 100
