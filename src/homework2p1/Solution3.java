package homework2p1;

import java.util.Arrays;
import java.util.Scanner;

/**
 * (1 балл) На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
 * вводится число X — элемент, который нужно добавить в массив, чтобы
 * сортировка в массиве сохранилась.
 * Необходимо вывести на экран индекс элемента массива, куда нужно добавить
 * X. Если в массиве уже есть число равное X, то X нужно поставить после уже
 * существующего.
 * Ограничения:
 * ● 0 < N < 100
 * ● -1000 < ai < 1000
 * ● -1000 < X < 1000
 * Пример:
 * Входные данные                  Выходные данные
 * 6 10 20 30 40 45 60
 * 12
 *                                  1
 * 5
 * -1 0 2 2 3 2 4
 *                                  2
 */

public class Solution3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

       int n = scanner.nextInt();

        int[] ai = new int[n];
        int[] aj = new int[n+1];


        //Заполним массив ai
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }

        int x = scanner.nextInt();

        System.arraycopy(ai, 0, aj, 0, ai.length);
        aj[ai.length] = x;
        Arrays.sort(aj);

        for (int i = 0; i < aj.length; i++) {
            if (aj[i] == x && aj[i] < aj[i+1]) {
                System.out.println(i);
            }
        }

    }
}
