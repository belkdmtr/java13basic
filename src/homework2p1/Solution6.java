package homework2p1;

import java.util.Scanner;

// П     Р      И       В       Е       Т
//•--•  •-•    ••      •--      •       -

/**
 * На вход подается строка S, состоящая только из русских заглавных
 * букв (без Ё).
 * Необходимо реализовать метод, который кодирует переданную строку с
 * помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв
 * нужно пробелом.
 */

public class Solution6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] morze = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---",
                "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.",
                "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        String s = scanner.nextLine();
        String res = "";


        //Цикл выводит считывает длину строки и по одной букве выводит букву в морфмате Морзе
        for (int i = 0; i < s.length(); i++) {
            //Выводим букву под индексом i приобразовав переменную i valueOf
            res += SelectMorze(String.valueOf(s.charAt(i)), morze) + " ";
        }

        //Выводим результат без пробела в конце
        System.out.print(res.substring(0, res.length() - 1));




    }
    public static String SelectMorze(String inStr, String[]inMorze) {
        int indexMass = 0;
        switch (inStr) {
            case ("А"):
                indexMass = 0;
                break;
            case ("Б"):
                indexMass = 1;
                break;
            case ("В"):
                indexMass = 2;
                break;
            case ("Г"):
                indexMass = 3;
                break;
            case ("Д"):
                indexMass = 4;
                break;
            case ("Е"):
                indexMass = 5;
                break;
            case ("Ж"):
                indexMass = 6;
                break;
            case ("З"):
                indexMass = 7;
                break;
            case ("И"):
                indexMass = 8;
                break;
            case ("Й"):
                indexMass = 9;
                break;
            case ("К"):
                indexMass = 10;
                break;
            case ("Л"):
                indexMass = 11;
                break;
            case ("М"):
                indexMass = 12;
                break;
            case ("Н"):
                indexMass = 13;
                break;
            case ("О"):
                indexMass = 14;
                break;
            case ("П"):
                indexMass = 15;
                break;
            case ("Р"):
                indexMass = 16;
                break;
            case ("С"):
                indexMass = 17;
                break;
            case ("Т"):
                indexMass = 18;
                break;
            case ("У"):
                indexMass = 19;
                break;
            case ("Ф"):
                indexMass = 20;
                break;
            case ("Х"):
                indexMass = 21;
                break;
            case ("Ц"):
                indexMass = 22;
                break;
            case ("Ч"):
                indexMass = 23;
                break;
            case ("Ш"):
                indexMass = 24;
                break;
            case ("Щ"):
                indexMass = 25;
                break;
            case ("Ъ"):
                indexMass = 26;
                break;
            case ("Ы"):
                indexMass = 27;
                break;
            case ("Ь"):
                indexMass = 28;
                break;
            case ("Э"):
                indexMass = 29;
                break;
            case ("Ю"):
                indexMass = 30;
                break;
            case ("Я"):
                indexMass = 31;
                break;



            /*case "А" -> indexMass = 0;
            case "Б" -> indexMass = 1;
            case "В" -> indexMass = 2;
            case "Г" -> indexMass = 3;
            case "Д" -> indexMass = 4;
            case "Е" -> indexMass = 5;
            case "Ж" -> indexMass = 6;
            case "З" -> indexMass = 7;
            case "И" -> indexMass = 8;
            case "Й" -> indexMass = 9;
            case "К" -> indexMass = 10;
            case "Л" -> indexMass = 11;
            case "М" -> indexMass = 12;
            case "Н" -> indexMass = 13;
            case "О" -> indexMass = 14;
            case "П" -> indexMass = 15;
            case "Р" -> indexMass = 16;
            case "С" -> indexMass = 17;
            case "Т" -> indexMass = 18;
            case "У" -> indexMass = 19;
            case "Ф" -> indexMass = 20;
            case "Х" -> indexMass = 21;
            case "Ц" -> indexMass = 22;
            case "Ч" -> indexMass = 23;
            case "Ш" -> indexMass = 24;
            case "Щ" -> indexMass = 25;
            case "Ъ" -> indexMass = 26;
            case "Ы" -> indexMass = 27;
            case "Ь" -> indexMass = 28;
            case "Э" -> indexMass = 29;
            case "Ю" -> indexMass = 30;
            case "Я" -> indexMass = 31;
            default -> indexMass = 99;*/
        }
        return inStr = inMorze[indexMass];
    }
}
