package homework2p1;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого передается число M — величина
 * сдвига.
 * Необходимо циклически сдвинуть элементы массива на M элементов вправо.
 * Ограничения:
 * ● 0 < N < 100
 * ● -1000 < ai < 1000
 * ● 0 <= M < 100
 * Пример:
 * Входные данные                       Выходные данные
 * 5
 * 38 44 0 -11 2                     -11 2 38 44 0
 * 2
 * 2
 * 12 15                                12 15
 * 0
 */

import java.util.Scanner;

public class Solution5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] ai = new int[n];
        String str = "";

        //Заполним массив ai
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }

        int m = scanner.nextInt(); //Сдвиг массива

        ReverseArray (ai, n, m); //МЕТОД СВДИГА МАССИВА на +N

        for (int i = 0; i < n; i++) {
           str += ai[i] + " ";
        }

        //Вывод отформатированный троки без пробела в конце
        System.out.print(str.substring(0, str.length() - 1));
    }


    public static void ReverseArray (int[] ai, int n, int m) {
        int i = 0;
        int j = n - m - 1; //Делаем ревер отрезка от 0 до m

        while (i < j) {
            int temp = ai[i];
            ai[i] = ai[j];
            ai[j] = temp;
            i++;
            j--;
        }

        //Делаем ревер отрезка от m + 1 до конца массива ai.length - 1;
        i = m + 1;
        j = ai.length - 1;
        while (i < j) {
            int temp = ai[i];
            ai[i] = ai[j];
            ai[j] = temp;
            i++;
            j--;
        }

        //Делаем ревер всего массива;
        i = 0;
        j = n - 1;

        while (i < j) {
            int temp = ai[i];
            ai[i] = ai[j];
            ai[j] = temp;
            i++;
            j--;
        }
    }

    /*public static void ShiftArray (int[] ai, int n, int m) {

        for (int i = 0; i < m; i++) {
            int temp = ai[n - 1];
            //int end = ai[m];
            for (int j = n - 1; j > 0; j--) {
                ai[j] = ai[j - 1];
                ai[0] = temp;
            }
        }
    }*/

}

//38 44 0 -11 2