package homework2p1;

import java.util.Objects;
import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * строк из N элементов (разделение через перевод строки). Каждая строка
 * содержит только строчные символы латинского алфавита.
 * Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
 * только один.
 * Ограничения:
 * ● 0 < N < 100
 * ● 0 < ai.length() < 1000
 *
 * Входные данные       Выходные данные
 * 4 hello
 * java
 * hi
 * java                 java
 *
 * 7 today
 * is
 * the
 * most
 * most
 * special
 * day                  most
 *
 *
 */

public class Solution9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        String str = "";
        Boolean flag = false;

        String [] arr = new String[n];
        //String[] arr = {"hello\n", "java\n", "hi\n", "java\n"};

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.next();
        }


        for (int i = 0; i < arr.length - 1; i++) {
            if (!flag)
           str = arr[i];
            else break;
           for (int j = i + 1; j < arr.length; j++) {
               if (Objects.equals(str, arr[j])) { // СРАВНЕНИЕ СТРОК
                   System.out.println(str);
                   flag = true;
               }
           }
        }


    }
}
