package week10.oop2.xmltags;

public interface IXmlFileStructure {
    
    XmlFirstTag fillFirstTag();
    
    void fillSecondTag();
    
    void fillNTag();
}
