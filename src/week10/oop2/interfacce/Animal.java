package week10.oop2.interfacce;

public interface Animal {

    String getColor();

    String getVoice();

    void eat(String food);
}
