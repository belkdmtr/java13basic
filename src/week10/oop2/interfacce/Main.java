package week10.oop2.interfacce;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Fox(14);
        animal.eat("кролик");
        System.out.println(animal.getColor());
        System.out.println(animal.getVoice());
        System.out.println(((Fox) animal).age());

        Animal animal1 = new Wolf();
        animal1.eat("");
        animal1.getColor();
        animal1.getVoice();
    }
}
