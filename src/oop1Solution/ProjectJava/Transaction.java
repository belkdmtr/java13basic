package oop1Solution.ProjectJava;

import java.util.Date;

//Создадим новый класс Transaction, в котором определим параметры транзакции банковского счета:

public class Transaction {

    //Добавим пять параметров для описания транзакции:
    private Date date;
    /** Тип транзакции, где + пополнение, - списание */
    private char type;
    private double amount;
    private double balance;
    private String description;

    //Добавим в класс конструктор с четырьмя параметрами,
    // значение дата транзакции определяется текущей датой и временем:
    public Transaction(char type, double amount, double balance, String description) {
        date = new Date();
        this.type = type;
        this.amount = amount;
        this.balance = balance;
        this.description = description;
    }

    //Добавим getter-методы для пяти полей класса Account :
    public Date getDate() {
        return date;
    }

    public char getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    public double getBalance() {
        return balance;
    }

    public String getDescription() {
        return description;
    }









}
