package oop1Solution.ProjectJava;

import java.util.Scanner;

/**
 *
 * Создадим новый класс, в котором смоделируем работу банкомата
 * Во второй части проекта с помощью банковского счета смоделируем работу банкомата.
 *
 * Используя уже созданный класс Account, смоделируйте работу банкомата.
 * Создайте 10 банковских счетов в массиве с идентификаторами (далее — id) 0, 1, …, 9
 * и начальным балансом 10 000 рублей. Система запрашивает у пользователя ввести id.
 * Если введен некорректный id, то попросите пользователя ввести корректный id.
 * После получения корректного id отображается главное меню, как показано в примере запуска.
 *
 * Можно ввести пункт меню
 * 1 для просмотра текущего баланса,
 * 2 — для снятия денег со счета,
 * 3 — для внесения денег на счет и
 * 4 — для выхода из основного меню.
 *
 * После выхода система снова запрашивает id, таким образом, после запуска она не останавливается.
 */


public class TestAccountATM {

    private static Scanner input = new Scanner(System.in);

    //Создадим массив для банковских счетов размером 10:
    private Account[] accounts = new Account[10];

    /**
     * В конструкторе с помощью цикла for заполним массив банковскими счетами
     * с идентификаторами от 0 до 9 и начальным балансом 10 000 рублей:
     */
    public TestAccountATM() {
        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = new Account(i, 10000);
        }
    }



    //МЕТОД MAIN
    public static void main(String[] args) {
        //Вторая часть задания
        //Создадим экземпляр созданного класса TestAccountATM
        TestAccountATM accountATM = new TestAccountATM();

        //Определим переменную, хранящую идентификатор счета, который выбрал пользователь
        int id;

        //Добавим бесконечный цикл
        while(true){
            System.out.print("Введите id: ");
            id = input.nextInt();
            if (id < 0 || id > accountATM.accounts.length) {
                System.out.println("ОШИБКА! Введите, корректный id");
                continue;
            }

            //Добавим цикл по выполнению действия с выбранным счетом:
            while (true) {
                int choice = accountATM.getMenuSelection();
                if (choice == 1)
                    System.out.println("Баланс равен " + accountATM.accounts[id].getBalance());
                else if (choice == 2)
                    accountATM.withdraw(id);
                else if (choice == 3)
                    accountATM.deposit(id);
                else
                    break;
            }//Внутренний цикл
        }//Внешний цикл
    }//КОНЕЦ МЕТОДА MAIN


    //Добавим метод, который отображает меню и получает один из его пунктов:
    public int getMenuSelection() {
        int choice;

        while (true) {
            System.out.println("\nОсновное меню");
            System.out.println("1: проверить баланс счета");
            System.out.println("2: снять со счета");
            System.out.println("3: положить на счет");
            System.out.println("4: выйти");
            System.out.print("Введите пункт меню: ");
            choice = input.nextInt();
            if (choice < 1 || choice > 4) {
                System.out.println("Вы ввели некорректный пункт меню!");
            }
            else {
                break;
            }
        }
        return choice;
    }

    //Добавим метод, который снимает с выбранного счета указанную сумму:
    public void withdraw (int id) {
        System.out.print("Введите сумму для снятия со счета: ");
        double amount = input.nextDouble();
        if (amount < 0) {
            System.out.print("Сумма отрицательная! Операция отменена.");
        } else if (amount <= accounts[id].getBalance()) {
            accounts[id].withdraw(amount);
        } else {
            System.out.print("Недостаточно средств на счете! Операция отменена.");
        }
    }

    //Добавим метод, который пополняет выбранный счет на указанную сумму:
    public void deposit(int id) {
        System.out.print("Введите сумму для внесения на счет: ");
        double amount = input.nextDouble();
        if (amount >= 0) {
            accounts[id].deposit(amount);
        }
        else {
            System.out.print("Сумма отрицательная! Операция отменена.");
        }
    }










}//КОНЕЦ КЛАССА TestAccountATM
