package oop1Solution.ProjectJava;

import java.util.ArrayList;
import java.util.Date;

public class Account {

    //Добавим в класс Account новое поле данных transactions типа ArrayList.
    // Этот список будет хранить объекты типа Transaction:
    private ArrayList<Transaction> transactions = new ArrayList<>();



    private int id; //(идентификатор) счета (по умолчанию равное 0).
    private double balance; //(остаток, баланс) счета (по умолчанию равное 0).
    private static double annualInterestRate; //процентная ставка (по умолчанию равная 0). Пусть у всех счетов она будет одинаковая.
    private Date dateCreated; //дата создания счета;

    //Добавим в класс Account новое поле данных name типа String, имя владельца счета:
    private String name;

    //Безаргументный конструктор, который создает счет с заданными по умолчанию значениями.
    /*public Account() {
        this.id = 0;
        this.balance = 0;
        //this.annualInterestRate = 0;
        dateCreated = new Date(); //???????
    }*/

    /**
     *Добавим конструктор, в котором будут определяться значения полей id и balance:
     */
    public Account (int id, double balance) {
        this.id = id;
        this.balance = balance;
        dateCreated = new Date(); //??????
    }

    //Добавим в класс Account новый конструктор с тремя параметрами:
    public Account(String name, int id, double balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        dateCreated = new java.util.Date();
    }

    //getter-методы для четырех полей класса:
    /** Возвращает id */
    public int getId() {
        return id;
    }

    /** Возвращает баланс */
    public double getBalance() {
        return balance;
    }

    /** Возвращает годовую процентную ставку */
    public static double getAnnualInterestRate() {
        return annualInterestRate;
    }

    /** Возвращает дату создания счета */
    public Date getDateCreated() {
        return dateCreated;
    }



    /**
     * Добавим в класс Account getter-методы для двух новых полей данных getName() и getTransactions():
     */
    public String getName() {
        return name;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }





    /**
     *setter-методы для трех полей, один из которых статический:
     */
    /** Присваивает новый id */
    public void setId(int id) {
        this.id = id;
    }

    /** Присваивает новый баланс */
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /** Присваивает новую годовую процентную ставку */
    public static void setAnnualInterestRate(double annualInterestRate) {
        Account.annualInterestRate = annualInterestRate;
    }

    /**
     * @return возвращает ежемесячный процент
     */
    public double getMonthlyInterest() {

        return balance * (annualInterestRate / 1200);
    }

    /**
     * снимает со счета указанную сумму
     * Изменим метод withdraw(), чтобы новая транзакция добавлялась в список транзакций:
     */
    public void withdraw (double amount) {

        balance -= amount;
        transactions.add(new Transaction('-', amount, balance, ""));
    }

    /**
     * пополняет со счета указанную сумму
     * Изменим метод deposit(), чтобы новая транзакция добавлялась в список транзакций:
     */
    public void deposit (double amount) {

        balance +=amount;
        transactions.add(new Transaction('+', amount, balance, ""));
    }
}
