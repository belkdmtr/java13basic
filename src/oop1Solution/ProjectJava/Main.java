package oop1Solution.ProjectJava;



public class Main {

    public static void main(String[] args) {

        //Первая часть задания
        Account account = new Account(1122, 20000); //ID клиента и баланс
        Account.setAnnualInterestRate(4.5); //Процентная ставка

        //Снимем деньги со счета
        account.withdraw(2500);

        //Пополним счет
        account.deposit(3000);

        System.out.println("Баланс счета: " + account.getBalance() + " руб");
        System.out.println("Ежемесячные процент равен: " + account.getMonthlyInterest());
        System.out.println("Дата создания счета: " + account.getDateCreated());
    }
}
