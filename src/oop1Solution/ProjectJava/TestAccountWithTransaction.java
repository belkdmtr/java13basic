package oop1Solution.ProjectJava;

import java.util.ArrayList;

/**
 * Создадим новый класс TestAccountWithTransaction,
 * в котором смоделируем работу банковских счетов с транзакциями:
 */

public class TestAccountWithTransaction {
    public static void main(String[] args) {

        //Установим значение годовой процентной ставки по счету
        //для класса Account через вызов статического метода:
        Account.setAnnualInterestRate(5.5);

        //Создадим объект типа Account с заданным именем клиента,
        // идентификатором и балансом банковского счета:
        Account account = new Account("Дмитрий", 1122, 10000);

        //Пополним созданный счет на 300, далее на 400, далее на 500 рублей соответственно:
        account.deposit(300);
        account.deposit(400);
        account.deposit(500);

        //Снимем с созданного счета 500, далее 400, далее 200 рублей соответственно:
        account.withdraw(500);
        account.withdraw(400);
        account.withdraw(200);

        //Выведем в консоль информацию по счету с указанием
        //имени владельца счета, процентной ставки и баланса:
        System.out.println("Имя: " + account.getName());
        System.out.println("Годовая процентная ставка: " + Account.getAnnualInterestRate());
        System.out.println("Баланс: " + account.getBalance());

        //Получим список транзакций по счету:
        ArrayList<Transaction> transactions = account.getTransactions();

        //Выведем в консоль информацию по транзакциям счета в виде таблицы с помощью цикла for each:
        System.out.println("\t\t\tДата\t\t\tТип транзакции\tСумма, руб.\tБаланс, руб.");

        /**
         * Для формирования строки воспользуемся методом format(), который избавит
         * от использования знака табуляции и позволит точно управлять промежутками
         * между значениями в столбцах. Знак % указывает на начало инструкции форматирования,
         * соответственно у нас их четыре по количеству выводимых параметров.
         * В конце %n означает перевод строки:
         */

        for (Transaction transaction : transactions) {
            System.out.format("%28s%7c%17.2f%14.2f%n", transaction.getDate(), transaction.getType(),
                    transaction.getAmount(), transaction.getBalance());
        }
    }
}
