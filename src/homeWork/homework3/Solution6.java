package homeWork.homework3;

//В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
//число n - количество денег для размена. Необходимо найти минимальное
//количество купюр с помощью которых можно разменять это количество денег
//(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
//вторым - 4 и т д)
//Ограничения:
//0 < n < 1000000

//51          6 0 1 1
//10          1 0 1 0
//60          7 1 0 0

import java.util.Scanner;

public class Solution6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] bank = {8, 4, 2, 1};

        int n = scanner.nextInt();

        if (n > 0 && n < 1000000) {
            for(int i = 0; i < bank.length; i++) {
                System.out.print(n / bank[i] + " ");
                n = n % bank[i];
            }
        }
    }
}
