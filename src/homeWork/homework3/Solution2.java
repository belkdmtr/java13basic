package homeWork.homework3;

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int sum = 0;

        if (m > 0 && n < 10 && m < n) {
            for (int i = m; i <= n; i++) {
                sum = m + sum;
                m++;
            }
            System.out.println(sum);
        }
    }
}
