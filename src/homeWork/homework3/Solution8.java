package homeWork.homework3;

//8. Вычислить сумму
//На вход подается:
//- целое число n
//- целое число p
//- целые числа a1, a2 , … an
//
//Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго больше p.
//
//Ограничение:
//0 < m, n, ai < 1000
//
//Примечание:
//Решение должно находиться в файле с названием Solution.java.
//Публичный класс с решением должен называться Solution.
//Использовать package нельзя.
//
//Пример входных данных
//2
//18
//95 31
//
//
//Пример выходных данных
//126


import java.util.Scanner;

public class Solution8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int res = 0;

        int n = scanner.nextInt();
        int p = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > p) {
                res +=arr[i];
            }
        }

        System.out.println(res);

    }
}
