package homeWork.homework3;

import java.util.Scanner;

public class Solution3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int res = 0;

        if (m > 0 && n < 10) {
            for (int i = 1; i <= n; i++) {
                res = res + (int)(Math.pow(m, i));

            }
            System.out.println(res);
        }
    }
}
