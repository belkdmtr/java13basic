package homeWork.homework3;

//10. Елочка
//Вывести на экран "ёлочку" из символа решетки (#) заданной высоты N.
//На N + 1 строке у "ёлочки" должен быть отображен ствол из символа |
//
//Ограничение:
//2 < n < 10
//
//Примечание:
//Решение должно находиться в файле с названием Solution.java.
//Публичный класс с решением должен называться Solution.
//Использовать package нельзя.
//
//Пример входных данных
//3
//
//
//Пример выходных данных
//    #
//   ###
//  #####
//    |

import java.util.Scanner;

public class Solution10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int count = 1;

        for (int i = 1; i <= n; i++) {
            for (int j = i; j < n; j++) {
                System.out.print(" ");
            }
            for (int j = i; j > 1; j--) {
                System.out.print("#");
            }
            for (int j = 1; j <= i; j++) {
                System.out.print("#");
                }
            System.out.println();
        }
       while (count <= n) {
           if (count == n)
               System.out.print("|");
           else
               System.out.print(" ");
           count++;
       }
    }
}
