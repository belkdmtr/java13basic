package homeWork.homework3;

import java.util.Scanner;

public class Solution5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt(); //Делимое.
        int n = scanner.nextInt(); //Делитель.

        int ost = n;
        int i = 0;
        int count = 0;

        if (m > 0 && n < 10) {
            if (m / n == m) {
                System.out.println(0);
            } else {
                if (m < n) {
                    System.out.println(m);
                } else {
                    if (m > n) {

                        while (i < m && m - i > n) {
                            ost = ost + n;
                            i += n;
                            count++;
                        }

                        System.out.println(m - i);

                    }
                }
            }
        }
    }
}
