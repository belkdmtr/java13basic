package homeWork.homework3;

//7. Количество символов
//Дана строка s.
//Вычислить количество символов в ней, не считая пробелов (необходимо использовать цикл).
//
//Ограничение:
//0 < s.length() < 1000
//
//Примечание:
//Решение должно находиться в файле с названием Solution.java.
//Публичный класс с решением должен называться Solution.
//Использовать package нельзя.
//
//Пример входных данных
//Hello world
//
//
//Пример выходных данных
//10

import java.util.Scanner;

public class Solution7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        int count = 0;

        for (int i = 0; i < s.length() - 1; i++) {
            if (s.charAt(i) == 32)
                count++;
        }
        System.out.println(s.length() - count);
    }
}
