package homeWork.homework1;

import java.util.Scanner;

public class Solution7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //System.out.print("Введите значение n: ");
        int n = scanner.nextInt();

        if (n > 9 && n < 100) {
            String str = n + ""; //Преобразуем int в строку.
            System.out.println(str.charAt(1) + "" + str.charAt(0)); //выводим сначала 2й символ, потом первый.
        }
    }
}
