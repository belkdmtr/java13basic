package homeWork.homework1;

import java.util.Scanner;

public class Solution3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count;

        //System.out.print("Введите имя пользователя: ");
        String userName = scanner.nextLine();
        count = userName.length();

        if (count > 0 && count < 100) {
            System.out.println("Привет, " + userName + "!");
        }
    }
}
