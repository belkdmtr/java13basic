package homeWork.homework1;

import java.util.Scanner;

public class Solution6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final double KM_PER_MIL = 1.60934;

        //System.out.print("Введите количество киллометров: ");
        double count = scanner.nextDouble();

        if (count > 0 && count < 1000) {
            System.out.println(count / KM_PER_MIL);
        }
    }
}
