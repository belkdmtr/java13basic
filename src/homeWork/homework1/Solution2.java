package homeWork.homework1;

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double s;

        //System.out.print("Введите переменную a=");
        double a = scanner.nextDouble();
        //System.out.print("Введите переменную b=");
        double b = scanner.nextDouble();

        s = Math.sqrt(1.0 * ((a * a) + (b * b)) / 2.0);

        if (a > 0 && b < 100) {
            System.out.println(s);
        }
    }
}
