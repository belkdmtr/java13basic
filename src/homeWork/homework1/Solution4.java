package homeWork.homework1;

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int count = scanner.nextInt();

        if (count > 0 && count < 86400) {
            System.out.println((count / 3600) % 60 + " "
                    + (count / 60) % 60);
        }
    }
}
