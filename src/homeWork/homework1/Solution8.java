package homeWork.homework1;

import java.util.Scanner;

public class Solution8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //System.out.print("Баланс счета в банке: ");
        double n = scanner.nextDouble();

        if (n > 0 && n < 100000) {
            n /= 30;
            System.out.println(n);
        }
    }
}
