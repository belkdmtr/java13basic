package homeWork.homework1;

import java.util.Scanner;

public class Solution5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //System.out.print("Количество дюймов: ");
        double count = scanner.nextDouble();

        if (count > 0 && count < 1000) {
            System.out.println(count * 2.54);
        }
    }
}
