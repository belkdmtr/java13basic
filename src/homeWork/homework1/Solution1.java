package homeWork.homework1;

import java.util.Scanner;

public class Solution1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double v; //Переменная обьема шара

        //System.out.print("Введите радиус шара: ");
        double r = scanner.nextDouble(); //Ввод радиуса шара с консоли

        if (r > 0 && r < 100) {
            v = (4.0 / 3.0) * Math.PI * Math.pow(r, 3);
            System.out.println(v);
        }
    }
}
