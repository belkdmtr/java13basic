package homeWork.homework1;

import java.util.Scanner;

public class Solution9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //System.out.print("Бюджет мероприятия: ");
        int n = scanner.nextInt();

        //System.out.print("Бюджет на одного гостя: ");
        int k = scanner.nextInt();

        if ((n > 0 && n < 100000) && (k > 0 && k < 1000) && (k < n)) {
            System.out.println(n / k);
        }
    }
}