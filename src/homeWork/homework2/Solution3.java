package homeWork.homework2;

import java.util.Scanner;

public class Solution3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n  = scanner.nextInt();

        if (n >= 0 && n <= 23) {
            if (n > 12) System.out.println("Пора");
            else System.out.println("Рано");
        }
    }
}
