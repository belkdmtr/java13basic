package homeWork.homework2;

import java.util.Scanner;

public class Solution14HARD {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String modelPhone = scanner.nextLine();
        int price = scanner.nextInt();
        if (modelPhone.contains("iphone") || modelPhone.contains("samsung")
                && price >= 50000 && price <= 120000) {
            System.out.println("Можно купить");
        } else
        {
            System.out.println("Не подходит");
        }
    }
}

//На вход подается строка – модель телефона и число – стоимость телефона.
//Нужно вывести "Можно купить", если модель содержит слово samsung или
//iphone и стоимость от 50000 до 120000 рублей включительно. Иначе вывести
//"Не подходит".
//Гарантируется, что в модели телефона не указано одновременно несколько
//серий.

//iphone XL
//58000
//Можно купить

//nokia 10
//100000
//Не подходит

//samsung Galaxy (GT-I7500)
//40000
//Не подходит

//samsung S 21
//120000
//Можно купить