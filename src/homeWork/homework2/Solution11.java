package homeWork.homework2;

//Чтобы из трех отрезков можно было составить треугольник,
// необходимо и достаточно, чтобы сумма длин любых двух
// отрезков была строго больше третьего.

//На вход подается три целых положительных числа – длины сторон
//треугольника. Нужно вывести true, если можно составить треугольник из этих
//сторон и false иначе.
//Ограничения:
//0 < a, b, c < 100

import java.util.Scanner;

public class Solution11 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int max = Math.max(Math.max(a, b), c);
        int min = Math.min(Math.max(a, b), c);
        int med = a + b + c - (max + min);

        if (a > 0 && b >0 && c > 0 && a < 100 && b < 100 && c < 100) {
            System.out.println(min + med > max);
        }
    }
}
