package homeWork.homework2;

//10. Логарифмическое тождество
//"А логарифмическое?" - не унималась дочь.
//
//Ограничение:
//-500 < n < 500
//
//Примечание:
//Решение должно находиться в файле с названием Solution.java.
//Публичный класс с решением должен называться Solution.
//Использовать package нельзя.
//
//Пример входных данных
//1.0
//
//
//Пример выходных данных
//true
//Напишите программу, которая проверяет, что log(e^n) == n для любого
//вещественного n.

import java.util.Scanner;

public class Solution10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double n = scanner.nextDouble();

        if (n > -500 && n < 500) {
            System.out.println( Math.log(Math.exp(n)) == n);
        }
    }
}
