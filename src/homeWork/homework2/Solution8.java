package homeWork.homework2;

//(1 балл) Раз так легко получается разделять по первому пробелу, Петя решил
//немного изменить предыдущую программу и теперь разделять строку по
//последнему пробелу.
//Ограничения:
//В строке гарантированно есть хотя бы один пробел
//Первый и последний символ строки гарантированно не пробел
//2 < s.length() < 100

import java.util.Scanner;

public class Solution8 {
    public static void main(String[] args) {
        String s;

        Scanner scanner = new Scanner(System.in);
        s = scanner.nextLine();

        int count = s.lastIndexOf(' ');
        int endStr = s.length() - 1;

        if (s.length() > 2 && s.length() < 100 && count != -1 && s.indexOf(0) != ' ' && count != endStr) {
            String fistLine = s.substring(0, count);
            String lastLine = s.substring(count + 1);
            System.out.println(fistLine + "\n" + lastLine);
        }
    }
}
