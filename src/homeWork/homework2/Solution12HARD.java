package homeWork.homework2;

//name.matches("[A-Z][a-z]{1,19}");
//        //DD.MM.YYYY (DD, MM, YYYY - цифры, без ограничений)
//        date.matches("[0-3][0-9]\.\d{2}\.\d{4}");
//        phone.matches("\+[0-9]{11}");
//        System.out.println("Hello");
//        email.matches("[A-Za-z0-9\-\_\*\.]+@[a-z0-9]+\.(com|ru)");

//1. Проверить пароль
//У Марата был взломан пароль.
//Он решил написать программу, которая проверяет его пароль на сложность.
//
//В интернете он узнал, что пароль должен отвечать следующим требованиям:
// пароль должен состоять из хотя бы 8 символов
//В пароле должны быть:
// заглавные буквы
// строчные символы
// числа
// специальные знаки (_-)
//
//Если пароль прошел проверку, то программа должна вывести в консоль строку пароль надежный, иначе строку: пароль не прошел проверку
//
//Примечание:
//Решение должно находиться в файле с названием Solution.java.
//Публичный класс с решением должен называться Solution.
//Использовать package нельзя.
//
//Пример входных данных
//Hello_22
//
//
//Пример выходных данных
//пароль надежный

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution12HARD {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String pas = scanner.next();
        String pasHighCase = pas.toLowerCase();
        String pasLowCase = pas.toUpperCase();

        //сравнивает шаблон строки pass на наличие цифр.
        Pattern pattern = Pattern.compile("[0-9_-]+");
        Matcher matcher = pattern.matcher(pas);
        //System.out.println(matcher.find());

        if (pas.length() > 7 && !pas.equals(pasHighCase) && !pas.equals(pasLowCase) && matcher.find()) {
           System.out.println("пароль надежный");
       } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}
