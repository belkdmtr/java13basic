package homeWork.homework2;

//9. Тригонометрическое тождество
//Пока Петя практиковался в работе со строками, к нему подбежала его дочь и спросила: "А правда ли, что тригонометрическое тождество (sin^2(x)+ cos^2(x) - 1 == 0) всегда-всегда выполняется?"
//
//Напишите программу, которая проверяет, что при любом x на входе тригонометрическое тождество будет выполняться (то есть будет выводить true при любом x).
//
//Ограничение:
//-1000 < x < 1000
//
//Примечание:
//Решение должно находиться в файле с названием Solution.java.
//Публичный класс с решением должен называться Solution.
//Использовать package нельзя.
//
//Пример входных данных
//90
//
//
//Пример выходных данных
//true

import java.util.Scanner;

public class Solution9 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double x = scanner.nextDouble();
        double si = Math.sin(x);
        double co = Math.cos(x);

       if (x > -1000 && x < 1000) {
            System.out.println((Math.pow(Math.round(si), 2) + Math.pow(Math.round(co), 2) - 1) == 0);
        }
    }
}
