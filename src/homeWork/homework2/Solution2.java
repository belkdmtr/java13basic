package homeWork.homework2;

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int x  = scanner.nextInt();
        int y  = scanner.nextInt();

        if (x > -100 && y < 100) {
            System.out.println(x > 0 && y > 0);
        }
    }
}
