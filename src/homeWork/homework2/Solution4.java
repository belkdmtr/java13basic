package homeWork.homework2;

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();

        if (n >= 1 && n <= 7) {
            if (n < 6) System.out.println(6 - n);
            else System.out.println("Ура, выходные!");
        }
    }
}
