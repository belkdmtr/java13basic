package homework3p2.Solution1;

import Learning2.Solution;

public class Library {

    //Обьявить массив bookArray;
    private static Book[] bookArray;
    private static int count; //считатет сколько раз сохранил книгу

    //Создаем массив классов Book = 5;
    public static void createBookArray() {
        bookArray = new Book[5];
        for (int i = 0; i < bookArray.length; i++) {
            bookArray[i] = new Book();
        }
    }

    //Сохраняет новую книгу, проверяет, есть ли доступное место для сохранения
    public static void saveBook(String nameOfBook, String authorOfBook) {
        boolean thereIsInLibrary = false;
        //Проверяем, есть ли такая книга в библиотеке
        for (int i = 0; i < bookArray.length; i++) {
            if (bookArray[i].nameOfBook() == nameOfBook) {
                thereIsInLibrary = true;
                System.out.println(" ОШИБКА! Такая книга есть в библиотеке: ");
            }
        }

        //Сохроняем книги
        if (thereIsInLibrary == false) { //Если есть такая книга в библиотеке, то пропускаем эту запись
            if (count < bookArray.length) {
                bookArray[count].setNameOfBook(nameOfBook, authorOfBook);
                count++;
                //thereIsInLibrary = false;
            } else {
                System.out.print("Достигнут объем книг в библиотеке \n");
            }
        }
    }

    //Получить книгу под индексом
    public static void getAllBook(int i) {
        System.out.print("Книга: " + bookArray[i].nameOfBook() + ", автор: " + bookArray[i].authorOfBook());
        System.out.println(" (книга в библиотеке: " + bookArray[i].getWhoTake() + ")");
    }

    //Найти и вернуть список книг по автору.
    public static void getNameOfAuthor(String author) {
        boolean thereIsBook = false;
        for (int i = 0; i < bookArray.length; i++) {
            if (bookArray[i].authorOfBook().equals(author)) {
                System.out.print("Книги автора: " + bookArray[i].authorOfBook() + " - " + bookArray[i].nameOfBook() + "\n");
                thereIsBook = true;
            }
        }
        if (!thereIsBook)
            System.out.println("Книг с автором \""  + author +  "\" нет в библиотеке.");
    }

    //Найти и вернуть книгу по названию
    public static void getNameOfBook(String nameOfBook) {
        boolean thereIsBook = false;
        for (int i = 0; i < bookArray.length; i++) {
            if (bookArray[i].nameOfBook().equals(nameOfBook)) {
                System.out.print("Найдена книга c названием: " + bookArray[i].nameOfBook() + " ее автор " + bookArray[i].authorOfBook() + "\n");
                thereIsBook = true;
            }
        }
        if (!thereIsBook)
            System.out.println("Такой книги \""  + nameOfBook +  "\" нет в библиотеке.");
    }


    //Удалить книгу из библиотеки по названию, если такая книга в принципе есть в
    //библиотеке и она в настоящий момент не одолжена.
    public static void deleteBook(String nameOfBook) {
        int delIndx = 0;
        boolean thereIsBook = false;

        //Поиск индекса книги для удаления
        for (int i = 0; i < bookArray.length; i++) {
            if (bookArray[i].nameOfBook().equals(nameOfBook) && bookArray[i].getWhoTake() == null) {
                delIndx = i;
                thereIsBook = true;
                System.out.println("Книга \"" + nameOfBook + "\" удалена");
            }
        }

        //Пересобираем массив, что бы заполнить удаленную книгу и в конце массива осовободить место
        if (thereIsBook) {
            for (int i = delIndx + 1; i < bookArray.length; i++) {
                bookArray[i - 1] = bookArray[i];
            }
        } else {
            System.out.println("Удалять нечег или книга у посетителя!");
        }

        //В конец массива добавили новый экземпляр класса, что бы патом в него записать новую книгу
        bookArray[bookArray.length - 1] = new Book();
    }

    //Передать книгу в руки пользователя
    public static void putBookVisitor(String nameOfBook , String nameVisitor) {
        boolean visitorNotTakeBook = true; //Пользователь не брал книг из списка?
        boolean bookExistsInLibrary = false;

            //Смотрим весь массив что бы понять брал этот пользователь книгу или нет
        for (int i = 0; i < bookArray.length; i++) {
            if (bookArray[i].getWhoTake() == nameVisitor)
                visitorNotTakeBook = false;
        }

        if (visitorNotTakeBook == true) {
            for (int i = 0; i < bookArray.length; i++) {
                //Но сначла исключить ошибку если данные = null и книга не на руках (не одолжена)
                if (bookArray[i].nameOfBook() != null && bookArray[i].getWhoTake() == null) {
                    if (bookArray[i].nameOfBook().equals(nameOfBook)) { //найдем искомую книгу
                        bookArray[i].putBook(nameVisitor); //запишем имя пользователя к этой книге.
                        bookExistsInLibrary = true; //подтверждаем, что книга есть в библиотеке для вывода сообщений
                    }
                }
            }
        } else {
            System.out.println("Пользователь " + nameVisitor + " уже имеет книгу на руках.");
        }

        //Выводим, что книги с названием "Тимон и Пумба" в библиотеке нет
        if (!bookExistsInLibrary && visitorNotTakeBook)
            System.out.println("Книги с названием \"" + nameOfBook + "\" в библиотеке нет");
    }

    //Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу.
    public static void removeBookFromVisitor(String nameOfBook) {
        for (int i = 0; i < bookArray.length; i++) {
            //Проверяем, только те книги где уже кто-то ее юзает
            if (bookArray[i].getWhoTake() != null) {
                //Когда книга найдена, мы удаляем ее от пользователя
                if (bookArray[i].nameOfBook().equals(nameOfBook)) {
                    bookArray[i].removeBook(); //Зануляем NULL ссылку на пользователя
                }
            }
        }
    }
} //КОНЕЦ КЛАССА
