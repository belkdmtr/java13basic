package homework3p2.Solution1;

public class Main {
    public static void main(String[] args) {
        //Создадим массив в библиотеки из 5 книг
       Library.createBookArray();
        //Подключим книги


        Library.saveBook("Тимур и его команда", "Гайдар А.");
        Library.saveBook("Чук и Гек", "Гайдар А.");
        Library.saveBook("Дети капитана Гранта", "Жюль Верн");
        Library.saveBook("Отцы и дети", "Тургенев И.");
        //Library.saveBook("Отцы и дети", "Тургенев И.");
        Library.saveBook("Ася", "Тургенев И.");

        //Весь список книг
        for (int i = 0; i < 5; i++) {
            Library.getAllBook(i);
        }

        System.out.println("****************ПОИСК ПО АВТОРУ****************");
        //Найти и вернуть список книг по автору.
        Library.getNameOfAuthor("Тургенев И."); //Такой автор есть
        Library.getNameOfAuthor("Пушкин А."); //Такого автора нет

        //Найти и вернуть книгу по названию.
        System.out.println("****************ПОИСК ПО НАЗВАНИЮ**************");
        Library.getNameOfBook("Чук и Гек"); //Такая книга есть
        Library.getNameOfBook("Хищник против Чужлго"); //Такой книни есть

        System.out.println("****************УДАЛЕНИЕ КНИГИ*****************");
        //Удалить книгу из библиотеки
        Library.deleteBook("Чук и Гек");
        System.out.println("********Теперь массив книг выглядит так********");
        //Весь список книг
        for (int i = 0; i < 5; i++) {
            Library.getAllBook(i);
        }

        //Одолжить книгу посетителю по названию, если выполнены все условия:
        //a. Она есть в библиотеке.
        //b. У посетителя сейчас нет книги.
        //c. Она не одолжена.
        //Проверка у кого книга на руках
        System.out.println("********Выдача книг посетителям********");
        Library.putBookVisitor("Тимур и его команда", "Дмитрий");
        Library.putBookVisitor("Дети капитана Гранта", "Сергей");
        Library.getAllBook(0); //проверяем состояние, кто взял книгу
        Library.getAllBook(1); //проверяем состояние, кто взял книгу

        //Дмитрий еще берет книгу (проверка на то что пользователь уже иммеет книгу
        Library.putBookVisitor("Чук и Гек", "Дмитрий");

        //Пользователь пытается брать не существующую книгу
        Library.putBookVisitor("Тимон и Пумба", "Петро");

        //ВОЗВРАЩАЕМ КНИГУ ОБРАТНО В БИБЛИОТЕКУ
        System.out.println("********Возврат книги обратно в бибилиотеку********");
        Library.removeBookFromVisitor("Тимур и его команда");
        Library.getAllBook(0); //проверяем состояние, кто взял книгу




























        //Создадим книгу








        //Получим книгу из библиотеки
        //System.out.println(library[0].getBookOfLibrary());







    }
}
