package homework3p2.Solution1;

public class Book {
    private String nameOfBook;
    private String authorOfBook;
    private Visitor whoTake = null;


    public Book() {
    }

    public void setNameOfBook(String nameOfBook, String authorOfBook) {
        this.nameOfBook = nameOfBook;
        this.authorOfBook = authorOfBook;
    }

    public String nameOfBook() {
        return nameOfBook;
    }

    public String authorOfBook() {
        return authorOfBook;
    }

    //Возвращаем имя пользователя взяшего книгу
    public String getWhoTake() {
        if (whoTake != null) { //возращает, только не нулевое значение
            return whoTake.getNameVisitor();
        }
        return null;
    }

   // Создаем ссылку на экземпляр класса посетителя взявшего книгу
    public void putBook (String name){
        this.whoTake = new Visitor(name);
    }

    //Удалить книгу у посетителя
    public void removeBook () {
        this.whoTake = null;
    }
}


