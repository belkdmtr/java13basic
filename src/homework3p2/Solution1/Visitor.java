package homework3p2.Solution1;

public class Visitor {

    private String nameVisitor;
    private Visitor whoTake = null;

    public Visitor(String name) {
        this.nameVisitor = name;
    }

    public String getNameVisitor() {
        return nameVisitor;
    }
}
