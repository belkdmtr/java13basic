package Learning;

public class Java2 {
    public static void main(String[] args) {
        final double EPSILON = 1E-14;
        double x = 1.0 - 0.1 - 0.1 - 0.1 - 0.1 - 0.1;
        if (Math.abs(x - 0.5) < EPSILON) //вычитание значений по модулю
            System.out.println(x + " приблизительно равен 0.5.");
    }
}
