package Learning;

import java.util.Scanner;

public class Java1 {
    public static void main(String[] args) {
     Scanner scanner = new Scanner(System.in);
     int score = scanner.nextInt();
     int pay = 0;
     if (score > 90) {
         pay *= 1.03;
         System.out.println("pay (>90) = " + pay);
     } else {
         pay *= 1.01;
         System.out.println("pay = " + pay);
     }
    }
}
