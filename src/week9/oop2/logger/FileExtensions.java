package week9.oop2.logger;

public interface FileExtensions {
    String CSV_FILE_EXTENSION = ".csv";
    String TXT_FILE_EXTENSION = ".txt";
}
