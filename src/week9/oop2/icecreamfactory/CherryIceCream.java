package week9.oop2.icecreamfactory;

public class CherryIceCream
      implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Cherry, Cream, Ice, Love");
    }
}
