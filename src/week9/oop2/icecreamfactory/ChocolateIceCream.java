package week9.oop2.icecreamfactory;

public class ChocolateIceCream implements IceCream{
    @Override
    public void printIngredients() {
        System.out.println("Chocolate, Cream, Ice");
    }
}
