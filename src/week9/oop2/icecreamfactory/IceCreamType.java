package week9.oop2.icecreamfactory;

public enum IceCreamType {
    CHERRY,
    CHOCOLATE,
    VANILLA
}
