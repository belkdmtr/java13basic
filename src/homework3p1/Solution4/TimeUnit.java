package homework3p1.Solution4;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TimeUnit {
    private int hour = 0;
    private int minute = 0;
    private int second = 0;
    private GregorianCalendar calendar = new GregorianCalendar();


    //Возможность создать TimeUnit, задав часы, минуты и секунды.
    public TimeUnit(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    //Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
    // должны проставиться нулевыми.
    public TimeUnit(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    //Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
    //должны проставиться нулевыми.
    public TimeUnit(int hour) {
        this.hour = hour;
    }

    public void getFormat() {
        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);

        //ЗАДАЧА 1 Вывести на экран установленное в классе время в формате hh:mm:ss
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        System.out.println("Время в формате hh:mm:ss: " + dateFormat.format(calendar.getTime()));

        //ЗАДАЧА 2 Вывести на экран установленное в классе время в 12-часовом формате
        //(используя hh:mm:ss am/pm)
        dateFormat = new SimpleDateFormat("hh:mm:ss aa");
        System.out.println("Время в 12-часовом формате (am/p): " + dateFormat.format(calendar.getTime()));
    }

    public void setNewTime(int hour, int minute, int second) {

        this.hour = calendar.get(Calendar.HOUR) + hour;
        this.minute = calendar.get(Calendar.MINUTE) + minute;
        this.second = calendar.get(Calendar.SECOND) + second;

    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    //Метод, который прибавляет переданное время к установленному в
    //TimeUnit (на вход передаются только часы, минуты и секунды).
    public void newTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);

        System.out.println("Прибавляет переданное время к установленному " + dateFormat.format(calendar.getTime()));
    }
}