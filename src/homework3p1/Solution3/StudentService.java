package homework3p1.Solution3;

import java.util.Arrays;
import java.util.Comparator;

public class StudentService {

    //Подключаем наш класс Студентов
    Student student = new Student();

    /**
     * принимает массив студентов (класс Student из
     * предыдущего задания), возвращает лучшего студента (т.е. который
     * имеет самый высокий средний балл). Если таких несколько — вывести
     * любого.
     */
    public void bestStudent(String[][] listStudent, int count) {
        int maxGradeStudent = 0;
        int inDx = 0;
        for (int i = 0; i < count; i++) {
            if (maxGradeStudent < Integer.parseInt(listStudent[i][1])) {
                maxGradeStudent = Integer.parseInt(listStudent[i][1]);
                inDx = i;
            }
        }
        System.out.println();
        System.out.print("~ Лучший студент " + listStudent[inDx][0] +
                " всего баллов " + listStudent[inDx][1] + " ~" + "\n");
        System.out.println();
    }

    /**
     * sortBySurname() — принимает массив студентов (класс Student из
     * предыдущего задания) и сортирует его по фамилии.
     */
    public void sortBySurname(String[][] listStudent, int count) {

        //Адская сортировка пузырьком по строкам двумерного массива (наверное был проще сопособ)
        String[][] temp = new String[count][2];
        for (int i = 0; i + 1 < count; i++) {
            for (int j = 0; j + 1 < count; j++) {
                //сравнивает первые буквы Фамилии, которая из них больше и сортирует пузырьком
                Character a = listStudent[i + 1][0].charAt(0);
                Character b = listStudent[j][0].charAt(0);
                if (a < b) {
                    //Третий круг ада для преноса строки двумерного массива во временный temp
                    for (int k = 0; k < 2; k ++) {
                        temp[i][k] = listStudent[i + 1][k];
                        listStudent[i + 1][k] = listStudent[j][k];
                        listStudent[j][k] = temp[i][k];
                    }
                }
            }
        }

        //Печатаем список студентов в возростающем порядке
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < 2; j ++) {
                System.out.print(listStudent[i][j] + " ");
            }
            System.out.println();
        }
    }
}