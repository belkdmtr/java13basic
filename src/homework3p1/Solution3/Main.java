package homework3p1.Solution3;

public class Main {
    public static void main(String[] args) {
        //Первый студент
        Student student = new Student(); //Подключили первый класс
        StudentService studentService = new StudentService(); //Подключили второй класс

        System.out.println("Полный список студентов:");
        System.out.println("------------------------------------");

        //Первый студент
        student.setName("Аркадий");
        student.setSurname("Укупник");
        student.setGrades(3);
        student.setGrades(2);
        student.setGrades(4);
        System.out.println(student.getSurname() + " " + student.getName() + " имеет общий балл " + student.getGPA());
        student.addNewStudentInList();

        //Второй студент
        student.setName("Жанна");
        student.setSurname("Агуззарова");
        student.setGrades(2);
        student.setGrades(2);
        student.setGrades(3);
        System.out.println(student.getSurname() + " " + student.getName() + " имеет общий балл " + student.getGPA());
        student.addNewStudentInList();

        //Третий студент
        student.setName("Дмитрий");
        student.setSurname("Белкин");
        student.setGrades(6);
        student.setGrades(5);
        student.setGrades(5);
        System.out.println(student.getSurname() + " " + student.getName() + " имеет общий балл " + student.getGPA());
        student.addNewStudentInList();

        //Четвертый студент
        student.setName("Иванов");
        student.setSurname("Иван");
        student.setGrades(5);
        student.setGrades(5);
        student.setGrades(5);
        System.out.println(student.getSurname() + " " + student.getName() + " имеет общий балл " + student.getGPA());
        student.addNewStudentInList();

        //Вызов класса bestStudent передаем ему массив из студентов и счетчик
        studentService.bestStudent(student.getListOfStudents(), student.getCount());

        System.out.println("Отсортированный список студентов: ");
        System.out.println("------------------------------------");
        //Выводит отсортированный список студентов
        studentService.sortBySurname(student.getListOfStudents(), student.getCount());
        System.out.println("------------------------------------");

    }
}
