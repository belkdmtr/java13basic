package homework3p1.Solution3;

public class Student {
    private String name;
    private String surname;
    private int[] grades = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //Оценки студента )))

    //Массив со списком студентов и средим баллом
    private String[][] listOfStudents = new String[10][2];
    private int count = 0; //Счетчик для массива студентов от 0 до 10

    public Student() {
    }

    //конструктор по умолчанию
    public Student (String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    //Устанавливает Имя.
    public void setName(String name) {
        this.name = name;
    }

    //Устанавливает Фамилию.
    public void setSurname(String surname) {
        this.surname = surname;
    }

    //Возвращает Имя.
    public String getName() {
        return name;
    }
    //Возврщает Фамилию.
    public String getSurname() {
        return surname;
    }

    //метод, добавляющий новую оценку в grades. Самая первая оценка
    //удаляется, новая сохраняется в конце массива (т.е.
    //массив сдвигается на 1 влево).
    public void setGrades (int newGrade) {
        for (int i = 0; i < 1; i++) { //Сдвиг влево на 1 шаг
            for (int j = 0; j < grades.length - 1; j++) { //Передвигаем элементы
                grades[j] = grades[j + 1];
            }
            grades[grades.length - 1] = newGrade; //Присвоим новую оценку в конец строки
        }
    }


    //Метод получения значений массива с оценками
    public int[] getGrades() {
        return grades;
    }

    //метод, возвращающий средний балл студента (рассчитывается как
    //среднее арифметическое от всех оценок в массиве grades)
    public int getGPA () {
        int gpa = 0;
        //gpa -= tempGPA;
        for (int i = 1; i < grades.length; i++) {
            gpa +=grades[i];
        }
        return gpa; //Возращает средний бал студента.
    }

    //Добавим нового студента в список массива
    public void addNewStudentInList() {
        if (count != 10) {
            listOfStudents[count][0] = surname + " " + name ; //В первый столбец добавим имя студента
            listOfStudents[count][1] = "" + getGPA(); //Во второй столбец добавим его среднюю оценку
            count++;
        } else {
           count = 0; //Иначе начнем заполнять массив снова
        }

        //Обнулим массив grades для нового студента, что бы правильно сичтался средний бал
        for (int i = 0; i < grades.length; i++) {
           grades[i] = 0;
        }
    }

    //Метод получения массива со студенами
    public String[][] getListOfStudents() {

        return listOfStudents;
    }

    //Метод получения счетчика "массива со студенами"
    public int getCount() {
        return count;
    }
}
