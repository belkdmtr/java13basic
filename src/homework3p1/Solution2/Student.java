package homework3p1.Solution2;

public class Student {
    private String name;
    private String surname;
    private int[] grades = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //Оценки студента c прошлого семестра:)))

    public Student() {
    }

    public Student (String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    //Устанавливает Имя.
    public void setName(String name) {
        this.name = name;
    }

    //Устанавливает Фамилию.
    public void setSurname(String surname) {
        this.surname = surname;
    }

    //Возвращает Имя.
    public String getName() {
        return name;
    }
    //Возврщает Фамилию.
    public String getSurname() {
        return surname;
    }

    //метод, добавляющий новую оценку в grades. Самая первая оценка
    //удаляется, новая сохраняется в конце массива (т.е.
    //массив сдвигается на 1 влево).
    public void setGrades (int newGrade) {
        for (int i = 0; i < 1; i++) { //Сдвиг влево на 1 шаг
            for (int j = 0; j < grades.length - 1; j++) { //Передвигаем элементы
                    grades[j] = grades[j + 1];
                }
            grades[grades.length - 1] = newGrade; //Присвоим новую оценку в конец строки
            }
        }


    //Метод получения значений массива
    public int[] getGrades() {
        return grades;
    }

    //метод, возвращающий средний балл студента (рассчитывается как
    //среднее арифметическое от всех оценок в массиве grades)
    public int getGPA () {
        int gpa = 0;
        for (int i = 1; i < grades.length; i++) {
            gpa +=grades[i];
        }
       return gpa; //Возращает средний бал студента.
    }
}
