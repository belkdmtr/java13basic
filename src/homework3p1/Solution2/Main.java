package homework3p1.Solution2;

/**
 * Прогрмма имитирует получение Имени Фамилии,
 * хранит массив 10 последних оценок студента,
 * позволяет добавлять новые оценки в массив и просматривать их,
 * а так же выводит средний балл за все оценки
 */

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static Student student = new Student();
    public static Scanner scanner =new Scanner(System.in);
    public static void main(String[] args) {

        System.out.print("Введите Имя ");
        student.setName(scanner.nextLine()); //Передаем Имя в Класс Студент

        System.out.print("Введите Фамилию ");
        student.setSurname(scanner.nextLine()); //Передаем Фамилию в Класс Студент

        System.out.println();

        System.out.println("Добро пожаловать студент " +  student.getName() + " " + student.getSurname());
        System.out.println("Ваш средний бал равен " + student.getGPA());

        //Небольшая логика для заполнения и вывода всех оценок в массив
        System.out.println("Желаете ввести новые оценки или вывести уже имеющиеся (ДА/НЕТ) ?");
        String question = scanner.nextLine();
        if (question.equalsIgnoreCase("ДА")) {
            addGrade();
        }

        //Выводим уже имеющиеся оценки и завершаем программу
        System.out.print("Список оценкок: " + Arrays.toString(student.getGrades()) + "\n");
        System.out.println("ДО НОВЫХ ВСТРЕЧ");

    }

    //Метод имитирующий программу для заполнения оценок
    public static void addGrade() {
        int addGrade; //Переменная для хранения оценок
        while (true) {
            System.out.print("Введите последнююю полученную оценку (для выхода введите -1): ");
            addGrade = scanner.nextInt();

            if (addGrade != -1) { //Если мы не ввели -1 то программа продолжает прием оценок
                student.setGrades(addGrade);
                System.out.print("Список оценток теперь выглядит так: "
                        + Arrays.toString(student.getGrades()) + " и средний бал равен " + student.getGPA() + "\n");
            } else break;
        }
    }
}
