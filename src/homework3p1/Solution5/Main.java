package homework3p1.Solution5;

/**
 * Необходимо реализовать класс DayOfWeek для хранения порядкового номера
 * дня недели (byte) и названия дня недели (String).
 * Затем в отдельном классе в методе main создать массив объектов DayOfWeek
 * длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
 * Sunday) и вывести значения массива объектов DayOfWeek на экран.
 * Пример вывода:
 * 1 Monday
 * 2 Tuesday
 * …7
 * Sunday
 */

public class Main {
    public static void main(String[] args) {

        //объявляется и создается массив из 7 объектов типа DayOfWeek:
        DayOfWeek[] dayOfWeeksArray = new DayOfWeek[7];

        //инициализации dayOfWeeksArray используцем цикл for:
        for (byte i = 0; i < dayOfWeeksArray.length; i++) {
            dayOfWeeksArray[i] = new DayOfWeek(i);
        }

        //Вывод результата
        for (byte i = 0; i < dayOfWeeksArray.length; i++) {
            System.out.println(dayOfWeeksArray[i].getNameOfDay());
        }














    }
}
