package homework3p1.Solution5;

public class DayOfWeek {

    private byte weekDay;
    private String nameOfDay;

    public DayOfWeek() {
        this.weekDay = 0;
        this.nameOfDay = "Не выбран день недели";
    }

    public DayOfWeek(byte weekDay) {
        switch (weekDay) {
            case (0):
                this.weekDay = ++weekDay;
                nameOfDay = "Понедельник";
                break;
            case (1):
                this.weekDay = ++weekDay;
                nameOfDay = "Вторник";
                break;
            case (2):
                this.weekDay = ++weekDay;
                nameOfDay = "Среда";
                break;
            case (3):
                this.weekDay = ++weekDay;
                nameOfDay = "Четверг";
                break;
            case (4):
                this.weekDay = ++weekDay;
                nameOfDay = "Пятница";
                break;
            case (5):
                this.weekDay =++weekDay;
                nameOfDay = "Субота";
                break;
            case (6):
                this.weekDay = ++weekDay;
                nameOfDay = "Восркесенье";
                break;
        }
    }

    public String getNameOfDay() {
        return weekDay + " " + nameOfDay;
    }
}
