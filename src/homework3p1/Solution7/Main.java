package homework3p1.Solution7;

/**
 * Реализовать класс TriangleChecker, статический метод которого принимает три
 * длины сторон треугольника и возвращает true, если возможно составить из них
 * треугольник, иначе false. Входные длины сторон треугольника — числа типа
 * Java 12 Базовый модуль Неделя 6
 * ДЗ 3 Часть 1
 * double. Придумать и написать в методе main несколько тестов для проверки
 * работоспособности класса (минимум один тест на результат true и один на
 * результат false)
 */

public class Main {
    public static void main(String[] args) {
        System.out.println(TriangleChecker.validateTriangle(3.0, 2.0, 1.0));
        System.out.println(TriangleChecker.validateTriangle(3.0, 4.0, 5.0));
        System.out.println(TriangleChecker.validateTriangle(3.0, 15.0, 15.0));
        System.out.println(TriangleChecker.validateTriangle(10.0, 2.0, 7.0));
    }
}
