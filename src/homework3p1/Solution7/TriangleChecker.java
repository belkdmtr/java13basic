package homework3p1.Solution7;

public class TriangleChecker {
    public static boolean validateTriangle(double a, double b, double c) {
        if ((Math.min(Math.max(a, b), c) + ((a + b + c) - (Math.max(Math.max(a, b), c)
                + Math.min(Math.max(a, b), c))) > Math.max(Math.max(a, b), c))) {
            return true;
        }
        return false;
    }
}