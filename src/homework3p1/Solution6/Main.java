package homework3p1.Solution6;

public class Main {
    public static void main(String[] args) {


       //Входные данные
        char[] arrayChar = {' ', 'A', 'r', 'r', 'a', 'y', ' ', 'c', 'h', 'a', 'r'};
        String string = " String is String";

        //Передали классу AmazingString массив
        AmazingString amazingString1 = new AmazingString(arrayChar);
        System.out.print("****** РАБОТА С МАССИВОМ CHAR ****** \n");
        //ЗАДАНИЕ 1 - Вернуть i-ый символ строки
        amazingString1.setSelectCharacter(5);
        System.out.println("Символ под номером 6 arrayChar, это буква: " + amazingString1.getCharacter());
        //ЗАДАНИЕ 2 - Вернуть длину строки
        System.out.println("Длина строки arrayChar: " + amazingString1.getLengthOfString() + " символов");
        //ЗАДАНИЕ 3 - Вывести строку на экран
        System.out.print("Строка: ");
        amazingString1.getString();
        System.out.println();

        //ЗАДАНИЕ 4
        //Проверить, есть ли переданная подстрока в AmazingString (на вход
        //подается массив char). Вернуть true, если найдена и false иначе
        System.out.println("Входит ли подстрока char в строку _Array_char: " + amazingString1.isThere("char"));

        //ЗАДАНИЕ 5
        //Удалить из строки AmazingString ведущие пробельные символы, если
        //они есть
        System.out.print("Вывод строки без пробела в начале: ");
        amazingString1.getStringWithoutSpace();

        //Задание 6
        //Развернуть строку (первый символ должен стать последним,
        // а последний первым и т.д.)
        System.out.println();
        System.out.print("Развернутая строка: ");
        amazingString1.getReverseString();
        System.out.println("\n");


        //*************************************************************************************
        //Передали классу AmazingString сроку " String is String"
        AmazingString amazingString2 = new AmazingString(string);
        System.out.print("****** РАБОТА СО СТРОКОЙ STRING ****** \n");

        //ЗАДАНИЕ 1 - Вернуть i-ый символ строки
        amazingString2.setSelectCharacter(5);
        System.out.println("Символ под номером 6 String, это буква: " + amazingString2.getCharacter());
        //ЗАДАНИЕ 2 - Вернуть длину строки
        System.out.println("Длина строки arrayChar: " + amazingString2.getLengthOfString() + " символов");
        //ЗАДАНИЕ 3 - Вывести строку на экран
        System.out.print("Строка: ");
        amazingString2.getString();
        System.out.println();

        //ЗАДАНИЕ 4
        //Проверить, есть ли переданная подстрока в AmazingString (на вход
        //подается массив char). Вернуть true, если найдена и false иначе
        System.out.println("Входит ли подстрока *g is S* в строку: " + amazingString2.isThere("g is Str"));

        //ЗАДАНИЕ 5
        //Удалить из строки AmazingString ведущие пробельные символы, если
        //они есть
        System.out.print("Вывод строки без пробела в начале: ");
        amazingString2.getStringWithoutSpace();

        //Задание 6
        //Развернуть строку (первый символ должен стать последним,
        // а последний первым и т.д.)
        System.out.println();
        System.out.print("Развернутая строка: ");
        amazingString2.getReverseString();
        System.out.println("\n");
    }
}

// P.S ХОТЯ МОЖНо бЫЛО И НЕ СОЗДАВАТЬ ЭКЗЕМПЛЯР КЛАССА amazingString1 amazingString2,
// но я это потом поня, так как у нас новый массив создался
