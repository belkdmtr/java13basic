package homework3p1.Solution6;

/**
 * Необходимо реализовать класс AmazingString, который хранит внутри себя
 * строку как массив char и предоставляет следующий функционал:
 * Конструкторы:
 * ● Создание AmazingString, принимая на вход массив char
 * ● Создание AmazingString, принимая на вход String
 * Публичные методы (названия методов, входные и выходные параметры
 * продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
 * не прибегая к переводу массива char в String и без использования стандартных
 * методов класса String.
 * ● Вернуть i-ый символ строки
 * ● Вернуть длину строки
 * ● Вывести строку на экран
 * ● Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается массив char). Вернуть true, если найдена и false иначе
 * ● Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается String). Вернуть true, если найдена и false иначе
 * ● Удалить из строки AmazingString ведущие пробельные символы, если
 * они есть
 * ● Развернуть строку (первый символ должен стать последним, а
 * последний первым и т.д.)
 */

public class AmazingString {
    private static char[] arrayChar; // Массив char входной
    private static char[] arrayString; //Входной массив для проверки
    private int selectCharacter; // переменная для хранения индекса искомого символа
    private boolean isThere;


    public AmazingString() {
        this.selectCharacter = 0;
        this.isThere = false;
    }

    //Создание AmazingString, принимая на вход массив char
    public AmazingString(char[] enterChar) {
        arrayChar = enterChar;  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

    //Создание AmazingString, принимая на вход String
    public AmazingString(String enterString) {
        arrayChar = new char[enterString.length()];

        for (int i = 0; i < arrayChar.length; i++) {
            arrayChar[i] = enterString.charAt(i);
        }

    }

    //Сеттер для передачи i-го символа строки, который нужно выбрать
    public void setSelectCharacter(int selectCharacter) {

        this.selectCharacter = selectCharacter;
    }

    //Возрвщает выбранный под номером setSelectCharacter символ из массива
    public char getCharacter() {
        return arrayChar[selectCharacter];
    }

    //Вернуть длину строки;
    public int getLengthOfString() {
        return arrayChar.length;
    }

    //Вывести строку на экран
    public void getString() {
        for (int i = 0; i < arrayChar.length; i++) {
            System.out.print(arrayChar[i]);
        }
    }

    /**
     * Удалить из строки AmazingString ведущие пробельные символы, если
     * они есть
     */
    public void getStringWithoutSpace() {
        int countSpace = 0;
        //Считаем, сколько пробелов в начале, что бы в будущем не поудалять пробелы в середине
        for (int i = 0; i < arrayChar.length; i++) {
            if (arrayChar[i] == 32) {
                countSpace++;
            } else break;
        }
        //Печатаем строчку за вычетом начальных пробелов
            for (int i = countSpace; i < arrayChar.length; i++) {
                    System.out.print(arrayChar[i]);
            }
    }

    /**
     * Развернуть строку (первый символ должен стать последним, а
     * последний первым и т.д.)
     */
    public void getReverseString() {
        char temp;
        for (int i = 1; i < arrayChar.length - i; i++) {
            temp = arrayChar[i - 1];
            arrayChar[i - 1] = arrayChar[arrayChar.length - i];
            arrayChar[arrayChar.length - i] = temp;
        }

        for (int i = 0; i < arrayChar.length; i++) {
            System.out.print(arrayChar[i]);
        }

    }

    //Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char).
    public boolean isThere(String inStr) {
        //Конвертер строки в массив char[] maskArray
        convertStringToCharArray(inStr);
        int count = arrayString.length; //счетчик хранения итераций, когда он будет равен длине массива arrayString то вхождение найдено
        int j = 1; //переменная для счетчика для массива arrayString

           if (arrayString.length > arrayChar.length) { //Если длинна входной строки больше массива то проверка не возможна
               return false;
           } else { //Иначе выполлняем логику по проверки вхождения одного массива в другой
               for (int i = 1; i < arrayChar.length ; i++) {
                   //Если символы arrayChar[j] и arrayString[0] совпали, начинаем проверку в методе checkingArray(i) с последующих символов,
                   //а так же исключаем выход за предлелы массива,если искомая строка находится в конце массива char
                       if (arrayChar[i - 1] == arrayString[0] && arrayString.length - 1 < arrayChar.length - i + 1) {
                           if (count == checkingArray(i)) { // если полное совпадение найдено то возвращаем тру
                               return true;
                           }
                       }
               }
           }
        return isThere;
    }



    // НИЖЕ ФУНКЦИОНАЛЬНЫЕ (ПРОМЕЖУТОЧНЫЕ) МЕТОДЫ
    //Конвертер строки в массив char
    private void convertStringToCharArray (String inputString) {
        arrayString = new char[inputString.length()];
        for (int i = 0; i < inputString.length(); i++) {
            arrayString[i] = inputString.charAt(i);
        }
        //return arrayEnter;
    }

    //Проверка на вхождение массива arrayString в массив arrayChar;
    private int checkingArray (int indexI) {
        int check = 1; //возвращаемое знгачение счетчика для поддтверждения полного вхождения
        for (int i = 1; i < arrayString.length; i++) { //проверяем последовательность символов
            if (arrayChar[indexI] == arrayString[i]) { // сравнение элементов массивов начиная со 2х элементов, так как первые мы сравнили при вызове метода
                check++;
            }
            indexI++;
        }
        return check;
    }
} // КОНЕЦ КЛАССА AmazingString
