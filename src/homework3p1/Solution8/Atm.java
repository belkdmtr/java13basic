package homework3p1.Solution8;

public class Atm {

    private double exchangeRate;
    private static int count;

    public Atm() {
        this.exchangeRate = 0;
        count++;
    }


    //Конструктор, позволяющий задать курс валют перевода
    //долларов в рубли и курс валют перевода рублей в доллары
    public void setExchangeRate(double exchangeRate) {
        if (exchangeRate > 0) {
            this.exchangeRate = exchangeRate;
        } else {
            System.out.print("Отрицательное занчение задавать нельзя");
        }
    }

    //Содержать два публичных метода, которые позволяют переводить
    //переданную сумму рублей в доллары и долларов в рубли
    public void convertRublesToDollars(double roubles) {
        System.out.println("Сумма " + roubles + " руб. в долларах равна " + Math.round(roubles / exchangeRate));
    }

    //Переводить переданную сумму долларов в рубли
    public void convertDollarsToRubles(double dollars) {
        System.out.println("Сумма " + dollars + " USD. в рублях равна " + Math.round(dollars * exchangeRate));
    }

    //Счетчик экземпляров класса
    public int getInstanceCount() {
        return count;
    }
}
