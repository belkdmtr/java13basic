package homework3p1.Solution8;

public class Main {
    public static void main(String[] args) {
        Atm atm = new Atm();
        //Зададим курс валют
        atm.setExchangeRate(61.5);
        //Перевести переданную сумму рублей в доллары
        atm.convertRublesToDollars(6000);
        //Перевести переданную сумму долларов в рубли
        atm.convertDollarsToRubles(97);
        //Метод, возвращающий счетчик
        System.out.println("Экземпляр Atm: " + atm.getInstanceCount());

        Atm atm2 = new Atm();
        //Зададим курс валют
        atm2.setExchangeRate(60.9);
        //Перевести переданную сумму рублей в доллары
        atm2.convertRublesToDollars(3000);
        //Перевести переданную сумму долларов в рубли
        atm2.convertDollarsToRubles(100);
        //Метод, возвращающий счетчик
        System.out.println("Экземпляр Atm: " + atm2.getInstanceCount());


        if (atm2 instanceof Atm) System.out.println("Да он экземпляр (Instance) Atm");
    }
}
